# Arduino Readme
* [Prerequisites](#prerequisites)
* [Installation](#installation)
* [Running](#running)

## Prerequisites
* Hardware
    * Arduino Due Board
    * USB micro to USB cable to plug into PC

* Software
  * [Arduino IDE](https://www.arduino.cc/en/software) - v 1.8.19 was used during project
  * [Arduino DUE specific prerequisites](#arduino-due-specific-prerequisites)

### Arduino DUE specific prerequisites
1. Open the Arduino IDE
2. Go to *Tools > Board > Board Manager*
3. In the window that pops up search "due" and select *Arduino SAM Boards(32-bits ARM Cortex-M3)* core
![img.png](img.png)
4. Once installed, Go to *Tools > Board: "Arduino Uno" > Arduino ARM (32-bits) Boards > Arduino Due (Programming Port)
![img_1.png](img_1.png)

## Installation
1. Make sure the Arduino is plugged in to the PC through the programming port (the usb micro port closest to the power jack) with the usb micro cable
2. Open up the IDE and the "xxxxxx.ino" sketches and hit the "verify" and "upload" buttons to load the firmware onto the board.

## Running
1. Make sure the Arduino is completely unplugged from the PC.
2. Use a USB micro cable to standard USB to plug serial port (furthest away from the power jack) to the Raspberry Pi.