#include <math.h>
#define ledPin 13
#define trigMeasPin 2
#define analogPin A0
#define analogPin2 A1
int timer1_counter;
int count = 0;
int pulseNo = 8;//4000; //first 2000 frames
unsigned int matchReg = 0; // in Hz
float freqIncr = 0.00;
bool completed = false;

int ADCReading = 0; //integer variable to hold the reading of the analogue pin
String ADCReadingArray = "";
//int* ADCReadingArray; //declare pointer for variable array size
//int ADCArraySize = 2; //set arbitrary array size

//SerialFunctions
char serInString[100];  // array that will hold the different bytes of the string. 100=100characters;
                        // -> you must state how long the array will be else it won't work properly
//read a string from the serial and store it in an array
//you must supply the array variable
void readSerialString (char *strArray) {
    int i = 0;
    if(Serial.available()>0) {    
      // Serial.print("reading Serial String: "); //optional: for confirmation
       while (Serial.available()>0){
          strArray[i] = Serial.read();
          i++;
         // Serial.print(strArray[(i-1)]);        //optional: for confirmation
       }
       //Serial.println();                        //optional: for confirmation
    }
}

//Print the whole string at once - will be performed only if thers is data inside it
//you must supply the array variable
void printSerialString(char *strArray) {
     int i=0;
     if (strArray[i] != 0) {     
         while(strArray[i] != 0) {
            Serial.print( strArray[i] );
            strArray[i] = 0;                  // optional: flush the content
            i++;
         }
     }
} 

//utility function to know wither an array is empty or not
boolean isStringEmpty(char *strArray) {
     if (strArray[0] == 0) {
         return true;
     } else {
         return false;
     }
}


void setup()
{
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  //ADCReadingArray =(int*)calloc(ADCArraySize,sizeof(int)); //allocate dynamic size

//https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  attachInterrupt(digitalPinToInterrupt(trigMeasPin), trigSigINT, CHANGE); //arduino uno pins 2 and 3 can have external interrupts

// //https://www.hobbytronics.co.uk/arduino-timer-interrupts 
//  // initialize timer1 
//  noInterrupts();           // disable all interrupts
//  TCCR1A = 0;
//  TCCR1B = 0;
//  // Set timer1_counter to the correct value for our interrupt interval
//  //calculation explained here: https://maker.pro/arduino/projects/timer-interrupts-improve-your-arduino-programming-skills
//  timer1_counter = calcTimerCounter(freqIncr);//65224; // 200Hz  
//  TCNT1 = timer1_counter;   // preload timer
//  TCCR1B |= (1 << CS12);    // 256 prescaler 
//  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
//  interrupts();             // enable all interrupts
}

ISR(TIMER1_OVF_vect)        // interrupt service routine 
{
  TCNT1 = matchReg;//timer1_counter;   // preload timer
  digitalWrite(ledPin, digitalRead(ledPin) ^ 1);
  //Serial.println(count);
 
}

void trigSigINT() //function executed when measured signal rising and falling edge?
{
  ADCReading = analogRead(analogPin); // read analog pin and store in variable
  // for plotting in arduino serial plotter
  Serial.print(ADCReading); 
  Serial.print("\t");
  Serial.println(analogRead(analogPin2));
  //Serial.println(ADCReadingArray[count]);
  count = count + 1;
 
  if(count >= pulseNo) // changed from >= to >
  {
   
    TIMSK1 |= (0 << TOIE1);   // disable timer overflow interrupt
    TCCR1A = 0;
    TCCR1B = 0;
    //completed = true;

    ADCReadingArray = ADCReadingArray + "," + ADCReading + ";";//ADCReading;
    //Serial.println(ADCReadingArray);
    //Serial.println("done");
    //completed = true;
    //count = 0;
  }
  else
  {
 
  if (count == 1)
  {
    ADCReadingArray = ADCReading ;//ADCReading;
    //Serial.println(ADCReadingArray);
  }
  else
  {
   ADCReadingArray = ADCReadingArray + "," + ADCReading ;//ADCReading;
   //Serial.println(ADCReadingArray);
  }
  //Serial.println(ADCReadingArray);

  //completed = false;
  }
}


void loop()
{
  // your program here...

  readSerialString(serInString);
  
  //do somenthing else perhaps wait for other data or read another Serial string
  //Serial.println ("------------ arduino is doing somenthing else ");
  
  if( isStringEmpty(serInString) == false) { //this check is optional
      //Serial.print("Arduino recorded that you said: ");
      //try to print out collected information. it will do it only if there actually is some info.

      float freq=0.00;
      //getCommands(serInString, freq, pulseNo);

      //example format for commands: 200,10; meaning 200 freq send 10 pulses
      String rawString = String(serInString);
      int ind1 = rawString.indexOf(',');  //finds location of first ,
      freq = (rawString.substring(0, ind1)).toFloat();   //captures first data String
      //Serial.print(rawString.substring(0, ind1));
      int ind2 = rawString.indexOf(';', ind1+1 );   //finds location of second ,
      pulseNo = (rawString.substring(ind1+1, ind2+1)).toInt();   //captures second data String 
      pulseNo *= 2; 

//      ADCArraySize = pulseNo;
//      ADCReadingArray =(int*)calloc(ADCArraySize,sizeof(int)); //allocate dynamic size
      
      digitalWrite(ledPin, LOW);
      matchReg = calcTimerCounter(freq);
     //matchReg = calcTimerCounter(atof(serInString));
      memset(serInString, 0, sizeof(serInString)); // empty serial data
      //readSerialString(serInString);
      //if( isStringEmpty(serInString) == false) {
      
        setPulses(matchReg);
      //  memset(serInString, 0, sizeof(serInString));  
     //}

//    if(completed == true)
//    {
//      for(int j=0; j<pulseNo; j++)
//      {
//        Serial.print(j);
//        Serial.print(":");
//        Serial.println(ADCReadingArray[j]);
//      }
//    }

//Serial.println(ADCReadingArray);
      
  }
//       if (completed == true){
//        Serial.print("done");        
//     }
  //slows down the visualization in the terminal
  delay(1000);
}


unsigned int calcTimerCounter(float freq)
{ 
  freq = freq*2.0; //because interrupt on rising and falling edge
 
//cap the frequency to 200Hz  
//  if (freq>400.00)
//  {
//    freq = 400.00;
//  }
 
  float matchRegVal = 0.00;
  float arduinoClkFreq = 16000000;
  float prescalar = 256.00;

  //TIMSK1 |= (0 << TOIE1);   // disable timer overflow interrupt

  matchRegVal = arduinoClkFreq/((prescalar*freq)-1.0);
  matchRegVal = round(matchRegVal);
  unsigned int timerCounter = 0;
  
  timerCounter = 65536 - matchRegVal;//because timer counts up to 65536

  float adjFreq = 0;

  adjFreq = ((arduinoClkFreq/matchRegVal) + 1.0)/prescalar;
  
  //Serial.print(freq/2.0,5);
  //Serial.print("\t");
  //Serial.print(adjFreq/2.0,5);
  Serial.println(adjFreq/2.0,5);

  return timerCounter;
}

void setPulses(unsigned int matchRegister)
{
   // noInterrupts();           // disable all interrupts
      TIMSK1 |= (0 << TOIE1);   // disable timer overflow interrupt
      TCCR1A = 0;
      TCCR1B = 0;

      digitalWrite(ledPin, LOW);
      ADCReadingArray = "";
      
      count = 0;

      
      // Set timer1_counter to the correct value for our interrupt interval
      //calculation explained here: https://maker.pro/arduino/projects/timer-interrupts-improve-your-arduino-programming-skills
      //calcTimerCounter(serInString.toFloat());//65224; // 200Hz  
      TCNT1 = matchRegister;   // preload timer
      TCCR1B |= (1 << CS12);    // 256 prescaler 
      TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
      //interrupts();             // enable all interrupts
}
//
//void getCommands(char rawChars[100], float freq, unsigned int noOfCycles)
//{
//  String rawString = String(rawChars);
//  int ind1 = rawString.indexOf(',');  //finds location of first ,
//  freq = (rawString.substring(0, ind1)).toFloat();   //captures first data String
//  Serial.print(rawString.substring(0, ind1));
//      //ind2 = readString.indexOf(',', ind1+1 );   //finds location of second ,
//      //fuel = readString.substring(ind1+1, ind2+1);   //captures second data String  
//}
