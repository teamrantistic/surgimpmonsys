int loop_count = 0;

float y = 0;

void setup() {
  //serialUSB is faster than Serial because not a virtual comms?
  SerialUSB.begin(115200);
}

void loop() {
  SerialUSB.flush();
  //sine wave generation
  for(int x=0; x<630; x++)
  {
    y = float(x)/10;
    SerialUSB.println((sin(y)*100) + 120);
  } 
  loop_count++;
}
