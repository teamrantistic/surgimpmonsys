//good source for possible tutorial: https://forum.arduino.cc/t/arduino-due-interrupt-adc-serial/670462
#include <DueTimer.h>

#define forceSensorPin A0
#define masterSigPin 12


int loop_count = 0;

float y = 0;


//interrupt handler functions
void timerInterHandler()
{
	//read ADC and print to usbSerial
  digitalWrite(2,HIGH);
	SerialUSB.println(String(analogRead(forceSensorPin))); // for some reason there needs to be other code for this line to work nicely inside the interrupt without an ADC bit flipping
}
void handler2()
{
  digitalWrite(masterSigPin, !digitalRead(masterSigPin));
}


void setup() {
  //setup force sensor ADC read resolution to 12-bit
  analogReadResolution(12); 

  //setup trigger signal pin
  pinMode(masterSigPin, OUTPUT);
  digitalWrite(masterSigPin, LOW);

  //serialUSB (native serial port) is faster than Serial because not a virtual comms?
  //initialise serial USB at 115200 baud
  SerialUSB.begin(115200);

  Timer4.attachInterrupt(handler2);
  Timer4.setFrequency(200);
  Timer4.start();

  //setup timer
  Timer3.attachInterrupt(timerInterHandler);
  Timer3.setFrequency(2000);
  Timer3.start();
  
  
}

void loop() {
  SerialUSB.flush();
//  SerialUSB.println(String(analogRead(forceSensorPin)));
//  digitalWrite(masterSigPin, !digitalRead(2));
//  //sine wave generation
//  for(int x=0; x<630; x++)
//  {
//    y = float(x)/10;
//    SerialUSB.println((sin(y)*100) + 120);
//  } 
  /*SerialUSB.println(String(analogRead(forceSensorPin)));*/

  loop_count++;
}
