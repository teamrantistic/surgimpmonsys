#include <DueTimer.h>

#define masterSigPin 12
#define cameraTrigPin 11
#define omniTrigPin 10
#define omniStartPin 8

#define electroMagPin 9 

#define sigReceivePin 2 // to verify that signal is generated and used to count pulses and send verification back to rpi

#define forceSensorPin A0
#define forceSensorRes 12 //resolution of force sensor in bits.

unsigned int forceSensorFreq = 1000; //frequency at which to query force sensor values in Hz
unsigned int cameraTrigFreq = 100; //frequency at which to trigger shutter, i.e. framerate in Hz should be a multiple of force Sensor Freq.
unsigned int omniTrigFreq = 50; // frequency at which to trigger omni through button press audio jack on stylus. 50Hz because of suspected debounce circuit.

unsigned int freqDivider = 10; //masterfreq/camerafreq

unsigned int forceSensorRead = 0;
unsigned int forceSensorReadArray[40000];

unsigned int pulseCount = 0; // count for internal purposes
unsigned int receivedPulseCount = 0; //variable to store no. of times the receive pin is interrupted
unsigned int lastRunPulseCount = 0;
unsigned int targetNoOfPulses = 200; // target no of camera frames

bool masterSig = false;
bool cameraTrig = false;
bool omniTrig = false;

bool magEngage = true;

bool modeContinuous = false;
bool finishedRun = false;

//serial communication variables for talking to rpi3
char serInString[100];

String serContinuousReadings = "";

int pulseHalfPeriod = 250; //(in microseconds)

int omniFreq = 50; //in Hz

void timerInterruptHandler() {

  if (pulseCount < targetNoOfPulses || modeContinuous == true)
  {
    masterSig = !masterSig;
    digitalWrite(masterSigPin, masterSig); // Led on, off, on, off...
    if (pulseCount % (forceSensorFreq / cameraTrigFreq) == 0)
    {
      if (pulseCount == 0)
      {
        cameraTrig = masterSig;
      }
      else
      {
        cameraTrig = !cameraTrig;
      }
      digitalWrite(cameraTrigPin, cameraTrig); // Led on, off, on, off...
    }

    if (pulseCount % (forceSensorFreq / omniTrigFreq) == 0)
    {
      if (pulseCount == 0)
      {
        omniTrig = masterSig;
      }
      else
      {
        omniTrig = !omniTrig;
      }
      digitalWrite(omniTrigPin, omniTrig); // Led on, off, on, off...
    }


    pulseCount = pulseCount + 1;

    if (modeContinuous == true)
    {
      Timer3.start(calcPulseMicroSeconds(forceSensorFreq));
    }

  }

  else // if the count has been reached in non-continuous mode
  {
    Timer3.stop();
    masterSig = false;
    cameraTrig = false;
    omniTrig = false;
    digitalWrite(masterSigPin, masterSig);
    digitalWrite(cameraTrigPin, cameraTrig);
    digitalWrite(omniTrigPin, omniTrig);
    
    pulseCount = 0;
    lastRunPulseCount = receivedPulseCount;
    receivedPulseCount = 0;
    //serContinuousReadings = "";
    finishedRun = true;
  }
}

void sigReceiveInterruptHandler() {
  //  if (receivedPulseCount < targetNoOfPulses)
  //  {
  receivedPulseCount = receivedPulseCount + 1;
  //Serial.println(analogRead(forceSensorPin));
  //forceSensorRead = analogRead(forceSensorPin);
  //forceSensorReadArray[receivedPulseCount] = forceSensorRead;
  if (receivedPulseCount <= targetNoOfPulses + 1)
  {
    if (receivedPulseCount == 1)
    {
      serContinuousReadings = "";
    }
    else if(receivedPulseCount == 2)
    {
      digitalWrite(electroMagPin, HIGH); //release hammer ... sig must be high for electromagnet to disengage
    }

    serContinuousReadings += String(receivedPulseCount) + "," + String(analogRead(forceSensorPin)) + ";"; //

  }
  //  else
  //  {
  //    finishedRun = true;
  //  }


  //  }

}

void setup() {

  //*forceSensorReadArray



  analogReadResolution(forceSensorRes); // set adc to 12-bit resolution

  pinMode(masterSigPin, OUTPUT);//master trigger
  pinMode(cameraTrigPin, OUTPUT); // for camera trigger
  pinMode(omniTrigPin, OUTPUT); // for omni trigger
   pinMode(omniStartPin, OUTPUT); // for omni trigger

  pinMode(electroMagPin, OUTPUT); //for electromagnet holding Hammer

  pinMode(sigReceivePin, INPUT);

  digitalWrite(masterSigPin, masterSig);
  digitalWrite(cameraTrigPin, cameraTrig);
  digitalWrite(omniTrigPin, omniTrig);
  digitalWrite(omniStartPin, LOW);

  digitalWrite(electroMagPin, LOW); // turn magnet on by writing pin low

  Timer3.attachInterrupt(timerInterruptHandler);
  attachInterrupt(sigReceivePin, sigReceiveInterruptHandler, CHANGE); // setup interrupt on rising and falling edge of signal
  Serial.begin(115200);
  //Timer3.start(pulseHalfPeriod); //1000 = 1ms(1kHz) or 500 = 0.5ms(2kHz)

}

void loop() {
  //  delay(1);
  //  for (int i = 0; i < targetNoOfPulses; i++)
  //  {
  //    Serial.println(forceSensorReadArray[i]);
  //  }
  //
  //  Timer3.start(calcPulseMicroSeconds(forceSensorFreq)); //1000 = 1ms(1kHz) or 500 = 0.5ms(2kHz)
  //Serial.println(receivedPulseCount);



  readSerialString(serInString);
  Serial.flush();

  if (isStringEmpty(serInString) == false) { //if not empty
    //example format for commands: 200,10; meaning 200 freq send 10 pulses

    //first 2 characters are reserved for command bytes
    char cmdMode = serInString[0];
    char cmdSensorSelect = serInString[1];
    //    int cmdMode = String(serInString[0]).toInt();
    //    int cmdSensorSelect = String(serInString[1]).toInt();

    String rawString = String(serInString);


    int msgPart1StartIndex = 2; //because first 2 characters reserved for commands
    int msgPart1EndIndex = rawString.indexOf(',', msgPart1StartIndex); // return the index of the first occurence of the comma after the command characters .. hence index position 2
    int msgPart2StartIndex = msgPart1EndIndex + 1;
    int msgPart2EndIndex = rawString.indexOf(';');

    String msgPart1 = rawString.substring(msgPart1StartIndex, msgPart1EndIndex);
    String msgPart2 = rawString.substring(msgPart2StartIndex, msgPart2EndIndex);

    //Serial.println(rawString);

    switch (cmdSensorSelect) {
      //int freq = msgPart1.toInt();

      case 'F': // if sensor select is forceSensor Freq
        forceSensorFreq = msgPart1.toInt();
        Serial.println("ForceSensor Query Freq Set to " + msgPart1 + "Hz");
        //Serial.flush();
        break;
      case 'C': //if sensor select is camera Freq
        cameraTrigFreq = msgPart1.toInt();
        Serial.println("Camera Trigger Freq Set to " + msgPart1 + "Hz");
        //Serial.flush();
        break;
      case 'P': //if sensor select is omni Freq
        omniTrigFreq = msgPart1.toInt();
        Serial.println("Omni Trigger Freq Set to " + msgPart1 + "Hz");
        //Serial.flush();
        break;
      default:
        //do nothing
        break;
    }

    switch (cmdMode) {
      case 'A': //single pulse capture for checkerboard
        modeContinuous = false;
        Serial.println("Mode: SingleShot");
        digitalWrite(cameraTrigPin, HIGH);
        delay(20);//20ms is 50Hz
        digitalWrite(cameraTrigPin, LOW);
        break;       
      case 'B': //burst
        digitalWrite(omniStartPin, HIGH); //for starting new phantom omni recording
        delay(1);
        digitalWrite(omniStartPin, LOW); //for starting new phantom omni recording
        delay(1);
        modeContinuous = false;
        targetNoOfPulses = (msgPart2.toInt()) * 2;
        serContinuousReadings = "";
        //digitalWrite(masterSigPin, HIGH);
        Serial.println("Mode: BurstMode with " + msgPart2 + "pulses");
        //Serial.flush();
        //delay(1);
        Timer3.start(calcPulseMicroSeconds(forceSensorFreq));
        break;
      case 'C'://continuos
        modeContinuous = true;
        //digitalWrite(masterSigPin, LOW);
        Serial.println("Mode: ContinuousMode");
        //Serial.flush();
        //delay(1);
        Timer3.start(calcPulseMicroSeconds(forceSensorFreq));
        break;
      case 'S'://stop
        Timer3.stop();
        modeContinuous = false;
      case 'R'://readings
        Serial.println(serContinuousReadings);
        //delay(1);
        Serial.println("EndRead");
        break;
      default:
        //do nothing
        Serial.println("Mode: NoModeSelected");
        break;
    }


  }

  memset(serInString, 0, sizeof(serInString)); //empty serial data

  //  if(receivedPulseCount%masterSigFreq)
  //  {
  //    if(receivedPulseCount!=0)
  //    {
  //      for (int i = 0; i < 2000; i++)
  //      {
  //        Serial.println(forceSensorReadArray[i]);
  //      }
  //    }
  //  //Serial.println(String(receivedPulseCount)+ String(forceSensorRead));
  //  }

  if (finishedRun == true)
  {
    digitalWrite(electroMagPin, LOW); // engage electromagnet
    finishedRun = false;
    //Serial.println(serContinuousReadings);
    Serial.println(lastRunPulseCount);
    Serial.flush();
  }
  Serial.flush();
  delay(10);
}




unsigned int calcDivider(int masterFreq, int cameraFreq)
{
  return (masterFreq / cameraFreq);
}

unsigned int calcPulseMicroSeconds(int masterFreq)
{
  float seconds = 1.0 / (float)masterFreq;
  int microSeconds = seconds * 1000000;
  return microSeconds / 2; // divide by 2 to because timer sets on and off.
}

//read a string from the serial and store it in an array
//you must supply the array variable
void readSerialString (char *strArray) {
  int i = 0;
  if (Serial.available() > 0) {
    // Serial.print("reading Serial String: "); //optional: for confirmation
    while (Serial.available() > 0) {
      strArray[i] = Serial.read();
      i++;
      // Serial.print(strArray[(i-1)]);        //optional: for confirmation
    }
    //Serial.println();                        //optional: for confirmation
  }
}

//Print the whole string at once - will be performed only if thers is data inside it
//you must supply the array variable
void printSerialString(char *strArray) {
  int i = 0;
  if (strArray[i] != 0) {
    while (strArray[i] != 0) {
      Serial.print( strArray[i] );
      strArray[i] = 0;                  // optional: flush the content
      i++;
    }
  }
}

//utility function to know wither an array is empty or not
boolean isStringEmpty(char *strArray) {
  if (strArray[0] == 0) {
    return true;
  } else {
    return false;
  }
}
