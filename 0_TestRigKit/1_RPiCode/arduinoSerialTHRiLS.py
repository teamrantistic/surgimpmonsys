import serial #pip install serial if not already installed
import time

def connectToArduino(platform, baudrate):
    if platform == 'laptop':
        # deviceAddress = 'COM3'
        # deviceAddress = 'COM8'
        deviceAddress = 'COM6'
    elif platform == 'PC':
        deviceAddress = 'COM8'
        # deviceAddress = 'COM5'
    elif platform == 'PC_TALBOT':
        deviceAddress = 'COM7'
    else: #if on raspberry pi
        deviceAddress = '/dev/ttyACM0'

    ser = serial.Serial(deviceAddress, baudrate, timeout=1)
    ser.flushInput()
    ser.flushOutput()
    time.sleep(5)  # THIS FIXES EVERYTHING!!!!!!
    if ser.isOpen():
        print('Ready to Comm with Arduino')
    else:
        print('Error, could not open serialport')

    return ser

def writeReadArduino(ser, cmd):
    if ser.is_open:
        ser.flush()
        ser.write(bytes(cmd, 'utf-8'))
        #ser.flushOutput()
        if ser.out_waiting == 0:
            time.sleep(0.05)
            if ser.in_waiting > 0:
                #time.sleep(0.05)
                data = ser.readline().decode('utf-8').rstrip()
                #time.sleep(0.05)
                data += " | " + ser.readline().decode('utf-8').rstrip()
                #ser.flushInput()
            else:
                data = 'did not receive reply from arduino'
        else:
            data = 'command not sent'
    else:
        data = 'serial port not open'
    return data

def readArduino(ser):
    current_timeout = ser.timeout
    ser.timeout = None
    data = ser.readline().decode('utf-8').rstrip()
    ser.timeout = current_timeout
    return data

def readArduinoContinuous(ser):
    data = ser.readline().decode('utf-8').rstrip()
    return data


# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    arduinoSerial = connectToArduino('PC',  115200)
    #arduinoSerial = connectToArduino('rpi',  115200)


    ## cmd list:
    #Byte0: burst or continuous mode
    #B- Burst mode followed by frequency of burst and no. of pulses
    #C - Continuous mode only requires a frequency .. no need for pulses
    #Byte1: which frequency to set - force sensor query freqency, camera trig frequency or omni trigger frequency
    #F - force sensor -default in arduino is 2000Hz
    #C - camera trigger - default is 200Hz in arduino
    #P-phantom omni - default is 50Hz in arduino
    cmdToSend = 'BF1000,4000;'

    arduinoResponse = writeReadArduino(arduinoSerial, cmdToSend)
    print(arduinoResponse)
    print(readArduino(arduinoSerial))
    time.sleep(1)
    cmdToSend = 'RN1000,4000;' #read values
    arduinoResponse = writeReadArduino(arduinoSerial, cmdToSend)
    print(arduinoResponse)
    # time.sleep(5)
    # cmdToSend = 'RF2000,200;'
    # print(writeReadArduino(arduinoSerial, cmdToSend))
    # print(writeReadArduino(arduinoSerial, cmdToSend))
    # print(writeReadArduino(arduinoSerial, cmdToSend))

   # # ser.timeout = None
   #  num = 1
   #  num = arduinoSerial.out_waiting
   #  if arduinoSerial.out_waiting < 1:
   #      print('not waiting')
   #      msgReceived = arduinoSerial.readline().decode('utf-8')
   #      print(msgReceived)
   #      msgReceived = arduinoSerial.readline().decode('utf-8')
   #      print(msgReceived)
   #  else:
   #      print('waitingToSend')
   # while True:
    #    time.sleep(1)
    #print(writeReadArduino(arduinoSerial,'R'))
