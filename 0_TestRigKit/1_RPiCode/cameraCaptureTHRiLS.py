import time
import os
import subprocess
import re

import arduinoSerialTHRiLS
import dataSaverTHRiLS
import omniUDPTHRiLS
import viewCameraAndCapture

from datetime import datetime

df = dataSaverTHRiLS.loadTestConfigValues()

delim1 = '_'
delim2 = '-'
delim3 = '/'
delim4 = '|'
leadingZeros = 2

captureStoreDevice = '/media/pi/DATASTORE/'

cameraModeDirectory = "/home/pi/cam-mipiov9281/Linux_4.19.118/ov9281/pi4/"

#captureStoreDevice = 'test/'
#testSelDirector = ''

# def set_camera_mode(mode):
#     #preview is 9, capture is 11
#     currDir = os.getcwd()#get current directory
#     os.chdir(cameraModeDirectory)#change directory to cameraModedirecotroy
#     return_code = subprocess.run(["make", "setmode"+str(mode)], capture_output=True)#run 'make setmode0'
#     #wait for output to go
#     os.chdir(currDir)#return to original directory

def runMainMenuLoop():
    while(1):
        print("\n--------------------------")
        print('\t' + df[['Option', 'TestType']].to_string(index=False))
        print("--------------------------")
        try:
            option = int(input('Enter which test to run: '))
            if option in df['Option'].values:
                print("...")
                return option
            else:
                raise ValueError
            break
        except ValueError:
            print('please input valid option and an integer only...')

def test_sel_handler(option):
    test_info = df.loc[df['Option'] == option]  ##pandas dataframe of option column
    test_directory = (test_info['Option'].to_string(index=False)).zfill(2) + "_" + test_info[
        'TestType'].to_string(index=False) + "/"
    test_date_directory = datetime.now().strftime("%y_%m_%d-%I_%M_%S_%p") + "/"
    return [test_directory, test_date_directory]

def loadAllVariables(option):
    test_info = df.loc[df['Option'] == option]  ##pandas dataframe of option column
    frequency = test_info['Frame-rate'].to_string(index=False)
    shutter = test_info['Shutter'].to_string(index=False)
    gain = test_info['Gain'].to_string(index=False)
    lighting = test_info['Lighting'].to_string(index=False)
    height = test_info['Height'].to_string(index=False)
    depth = test_info['Depth'].to_string(index=False)
    duration = test_info['DurationOfCapture(s)'].to_string(index=False)
    repeats = test_info['RepeatNo'].to_string(index=False)

    return[frequency,shutter,gain,lighting,height,depth,duration,repeats]

def set_camera_params(frequency, shutter, gain):
    ##make sure shutter values are within the  specified ranges for ov9281
    shutter_mult = int(shutter)
    if shutter_mult > 855:
        shutter_mult = 855
    elif shutter_mult < 1:
        shutter_mult = 1

    ##shutter values are in multiples of 8721ns minimum to 8721ns*855

    shutter_time = 8721.0 #nanoseconds multiples

    period = 1.0/float(frequency) # value in seconds
    period_ns = period*1000000000.0 # convert to nanoseconds
    shutter_ns = int(shutter_mult * shutter_time) # multiply shutter
    shutter_max_mult = int(period_ns/shutter_time) #maximum integer value of the multiple

    if shutter_ns >= period_ns:
        shutter_ns = int((shutter_max_mult-1) * shutter_time) ## minus one because we don'e want it to be the full width of the frame period

    ##check gain is in the value range of decibels described by ov9281 datasheet
    gain_db = int(gain)
    if gain_db < 0:
        gain_db = 0
    elif gain_db > 254:
        gain_db = 254

    ##convert integer to hex value
    gain_hex = hex(gain_db)

    return [str(shutter_ns), str(gain_hex)]

    #shutter_mult = shutter #shutter multiple 855

def clear_prev_captures():
    ##delete any existing image files
    list_of_files = os.listdir("/mnt/ramdisk")
    for frame in list_of_files:
        if frame.endswith(".pgm") or frame.endswith(".csv"):
            os.remove(os.path.join("/mnt/ramdisk/", frame))

def creatDir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
        print(dir)

# def runSingleShotCap(dir, freq, dur, shut, gai, light,height,depth):
#
#     ardSer = arduinoSerialTHRiLS.connectToArduino('rpi', 115200)
#     time.sleep(1)
#     omniUDPTHRiLS.udp_init()
#     # openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", "261630", "-g", "0x88", "-f", "-b", "32", "-a", "-o"],
#     #                                        stdout=None, stderr=None)
#     ## -s shutter value, -g gain, -f output capture to frame buffer /dev/fb0, -b buffer count to use, -o output capture to .pgm, -a supress ascii capture at stdout,
#     #shut = str(int(shut)*2)
#     #gai=str(hex(200))
#     # openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", "-b", "32", "-o", "-a"],
#     #                                        stdout=None, stderr=None)
#
#     while(1):
#         viewCameraAndCapture.set_camera_mode(9)  ##ready for preview
#         openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", ">/dev/null", "-a"], stdout=None,
#                                                stderr=None)
#         user_confirm = input("Checkerboard in place? Start recording? y/n: ")
#         clear_prev_captures()
#         time.sleep(0.5)
#
#         openCamChildProcess.terminate()
#         viewCameraAndCapture.set_camera_mode(7)
#         openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", "-b", "32", "-o", "-a"],
#                                                stdout=None, stderr=None)
#         dataSaverTHRiLS.creatDir(dir)
#
#         if user_confirm == 'n':
#             time.sleep(0.5)
#             print('terminating process ...')
#             openCamChildProcess.terminate()
#             print('exiting recording ...')
#             time.sleep(0.5)
#             exit()
#
#         print("capturing 1 shot.... ")
#         #    freq_req = input("please input test freq and no. of pulses ... freq,pulses;: ")
#         # print (freq_req)
#         # time.sleep(1)
#
#         noOfPulsesTotal = int(freq) * int(dur)
#         multiplier = 10
#
#         cmdToSend = 'AF' + str(int(freq) * multiplier) + ',' + str(noOfPulsesTotal * multiplier) + ';'  ##bf1000,4000
#         #    cmdToSend = 'BC10,20;'
#         time.sleep(1)
#
#         arduinoResponse = arduinoSerialTHRiLS.writeReadArduino(ardSer, cmdToSend)
#
#         print(arduinoResponse)
#         # time.sleep(0.5)
#
#         time.sleep(1)

def variableDictAssign(v_name, v_val, freq0, shut0, gain0, lighting0, height0, depth0):
    freq1 = freq0
    shut1 = shut0
    gain1 = gain0
    lighting1 = lighting0
    height1 = height0
    depth1 = depth0
    v_val = v_val.lstrip('0')##remove leading zeros in string
    if v_name == 'Frame-rate':
        freq1 = v_val
        #return freq1
    elif v_name == 'Shutter':
        shut1 = v_val
        #return shut1
    elif v_name == 'Gain':
        gain1 = v_val
    elif v_name == 'Lighting':
        lighting1 = v_val
    elif v_name == 'Height':
        height1 = v_val
    elif v_name == 'Depth':
        depth1 = v_val
        #return gain1
    return [freq1, shut1, gain1, lighting1, height1, depth1]

def runRecording(dir, freq, dur, shut, gai, light,height,depth): ##frequency duration shutter and gain
    ##run vcmipidemo
    count = 0



    while(1):
        print("CurrentTestDir: "+dir)
        user_confirm  = input("Sskip this test? s: ")
        
        if user_confirm =='s':
            time.sleep(0.5)
            print('skipping this test ...')
            #openCamChildProcess.terminate()
            break ##exit this while loop and got to next test


        ##serial
        ardSer = arduinoSerialTHRiLS.connectToArduino('rpi', 115200)
        time.sleep(1)
        ##send new file pulse
        cmdToSend = "NF1000,4000;"
        arduinoResponse = arduinoSerialTHRiLS.writeReadArduino(ardSer, cmdToSend)
        print(arduinoResponse)


        ##OMNI UDP
        #client_msg = ""
       #while(client_msg != "Omni Conn Live"): # wait for client message



        client_msg, client_addr = omniUDPTHRiLS.udp_receive()
        print(client_msg)
            #if count > 0:
        if client_msg == "Omni Conn Live":
            # on first data receive
            omniUDPTHRiLS.udp_send(client_addr, dir.replace(delim3, delim1))
            client_msg = ""


        ## -s shutter value, -g gain, -f output capture to frame buffer /dev/fb0, -b buffer count to use, -o output capture to .pgm, -a supress ascii capture at stdout,

        viewCameraAndCapture.set_camera_mode(9) ##ready for preview
        openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", ">/dev/null"], stdout=None, stderr=None)

        user_confirm = input("Set the Hammer to start position ... Start recording? y/n: ")

        openCamChildProcess.terminate()
        viewCameraAndCapture.set_camera_mode(7) ## ready for capture at higher resolution
        openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", "-b", "32", "-o", "-a"],
                                               stdout=None, stderr=None)

        if user_confirm =='n':
            time.sleep(0.5)
            print('terminating process ...')
            openCamChildProcess.terminate()
            print('exiting recording ...')
            time.sleep(0.5)
            exit()

        clear_prev_captures()
        time.sleep(0.5)


        dataSaverTHRiLS.creatDir(dir)


        print("starting Recording.... ")
        #    freq_req = input("please input test freq and no. of pulses ... freq,pulses;: ")
        # print (freq_req)
        # time.sleep(1)

        noOfPulsesTotal = int(freq)*int(dur)
        multiplier = 10



        cmdToSend = 'BF'+str(int(freq)*multiplier)+','+str(noOfPulsesTotal*multiplier)+';' ##bf1000,4000
        #    cmdToSend = 'BC10,20;'
        time.sleep(1)

        arduinoResponse = arduinoSerialTHRiLS.writeReadArduino(ardSer, cmdToSend)

        print(arduinoResponse)
        #time.sleep(0.5)

        totalNoOfPulses = arduinoSerialTHRiLS.readArduino(ardSer)
        
        # forceSensorData = arduinoSerialTHRiLS.readArduino(ardSer)

        print(totalNoOfPulses)

        time.sleep(1)
        #    time.sleep(4)

        #    print(arduinoSerialTHRiLS.readArduino(ardSer))

        # Stop vcmipidemo
        openCamChildProcess.terminate()

        frameCount = 0
        list_of_files = os.listdir("/mnt/ramdisk")
        for frame in list_of_files:
            if frame.endswith(".pgm"):
                frameCount = frameCount + 1

        droppedFrames = noOfPulsesTotal - frameCount
        # dropCountArray[j] = droppedFrames
        #        dropCountArray[j+1] = droppedFrames

        print("droppedFrames: " + str(droppedFrames))
        
        currdir  = str('/mnt/ramdisk/*.pgm')
        cpydir = dir

        if droppedFrames == 0:
            print("Capture Successful! Saving Data to DATASTORE ...")
            forceSensorData = arduinoSerialTHRiLS.writeReadArduino(ardSer, "RR1000,4000")
            dataSaverTHRiLS.saveForcSensorData(forceSensorData, cpydir+'ForceSensorData.csv')
            copy_result = subprocess.run('cp /mnt/ramdisk/*.pgm ' + cpydir, shell=True)
            print("copy errors? : %d" % copy_result.returncode)
            if copy_result.returncode != 0:
                print("could not copy images! .. exiting with error: ", copy_result)
                exit()
            print("saving complete!\n")
            count = 0
            break
        else:
            #forceSensorData = arduinoSerialTHRiLS.writeReadArduino(ardSer, "RR1000,4000")
            #dataSaverTHRiLS.saveTestSuccessFail(dir + '/Fails.csv', dir)
            # client_msg, client_addr = omniUDPTHRiLS.udp_receive() # reading to empty buffer
            # print(client_msg)
            # while client_msg != "":
            #     client_msg, client_addr = omniUDPTHRiLS.udp_receive()  # reading to empty buffer
            #print(client_msg)
            #count = count + 1
            print('Test Failed to capture All Frames: trying again ...\n')


if __name__ == '__main__':

    # ##This code is to be only to be run on RPi
    # ##connect to arduino through serial port
    #
    #
    # ##attempt to change directory to ramdisk Memory. Exit if cannot
    ##connect to udp
    omniUDPTHRiLS.udp_init()

    result = os.chdir("/mnt/ramdisk")
    if result != None:
        print("could not execute successfully error: ", result)
        exit()
    folder_contents = subprocess.run(["ls"], capture_output=True, text=True)

    if re.search("vcmipidemo", folder_contents.stdout):
        print("vcmipidemo already exists...skipping copy")
    else:
        copy_result = subprocess.run(
            ["cp", "/home/pi/surgimpmonsys/0_TestRigKit/1_RPiCode/vcmipidemo",
             "/mnt/ramdisk/"])
        print("copy errors? : %d" % copy_result.returncode)
        if copy_result.returncode != 0:
            print("could not copy! .. exiting with error: ", copy_result)
            exit()

    option = runMainMenuLoop()

    exp_name, exp_uid= test_sel_handler(option) ##experiment name for type of experiment and uid -> i.e. the date of the experiment
    freq, shut, gai, light, height, depth, dur, exp_rep_no = loadAllVariables(option)

    # setCorrectNumberOFTests(freq,'Frame-rate')
    # setCorrectNumberOFTests(shut, 'Shutter')
    # setCorrectNumberOFTests(gai, 'Gain')

    path = captureStoreDevice+exp_name+exp_uid
    creatDir(path) ##master directory for all variations of 1 capture block

    pathStart = captureStoreDevice+exp_name+exp_uid

    folder_depth, addressList = dataSaverTHRiLS.createFolderStructure(df,option,pathStart)

    # freq=10
    # # shut=10
    # shut = 261630
    # gai = '0x88'

    for folder_address in addressList:
        if folder_depth > 0:
            folder_hierarchy = folder_address.split(delim3)
            var1_position = len(folder_hierarchy)-2-folder_depth
            var1_name, var1_val = folder_hierarchy[var1_position].split(delim1)
            freq,shut,gai,light, height, depth = variableDictAssign(var1_name, var1_val,freq,shut,gai,light, height, depth)
            if folder_depth > 1:
                var2_position = var1_position + 1
                var2_name, var2_val = folder_hierarchy[var2_position].split(delim1)
                freq, shut, gai,light, height, depth = variableDictAssign(var2_name, var2_val,freq,shut,gai,light, height, depth)

        shut_str, gai_hex = set_camera_params(freq,shut,gai)
        if option != 0:
            runRecording(folder_address,freq,dur,shut_str,gai_hex, light, height, depth)
        else:
            print("Running viewCameraAndCapture...")
            viewCameraAndCapture.main()
            # singleCap_subprocess = subprocess.Popen(["python", "viewCameraAndCapture.py"], stdout=subprocess.PIPE)
        #     runSingleShotCap(folder_address,freq,dur,shut_str,gai_hex, light, height, depth)




