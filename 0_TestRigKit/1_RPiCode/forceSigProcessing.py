import forceVerification
from scipy import signal
from scipy.fft import fft, fftfreq, rfft, rfftfreq
import matplotlib.pyplot as plt
import numpy as np
import plotly.express as px
import pandas as pd

if __name__ == '__main__':
    x_raw, y_raw, fig_raw = forceVerification.plotForceData(forceVerification.foldername+'07-04-22_14-11-26_AD623Again3k2.csv')

    # print(y_raw)
    #
    # ##get frequency domain of force sensor data:
    #
    #
    # #
    # # b,a = signal.butter(4,5000,'low', analog=True)
    # # w, h = signal.freqs(b,a)
    # # plt.semilogx(w, 20 * np.log10(abs(h)))
    # # plt.title('Butterworth filter frequency response')
    # # plt.xlabel('Frequency [radians / second]')
    # # plt.ylabel('Amplitude [dB]')
    # # plt.margins(0, 0.1)
    # # plt.grid(which='both', axis='both')
    # # plt.axvline(100, color='green')  # cutoff frequency
    # # plt.show()
    #

    windowend = 10000

    N = len(x_raw[:windowend]) ## number of samples
    yf = rfft(y_raw[:windowend].to_numpy())
    xf = rfftfreq(N, 1/2000)
    # yf = fft(y_raw[:2000].to_numpy())
    # xf = fftfreq(2000, 1/2000)
    #
    fig2 = px.line(x=xf, y=np.abs(yf))
    # print(y_filtered)
    fig2.show()

    sos = signal.butter(2,40,'low', fs=2000, output='sos')
    y_filtered = signal.sosfilt(sos, y_raw)

    yf = rfft(y_filtered[:windowend])
    xf = rfftfreq(N, 1/2000)
    # yf = fft(y_raw[:2000].to_numpy())
    # xf = fftfreq(2000, 1/2000)
    #
    fig3 = px.line(x=xf, y=np.abs(yf))
    # print(y_filtered)
    fig3.show()

    # fig = px.line(x=x_raw[:2000], y=y_raw[:2000])
    fig4 = px.line(x=x_raw, y=y_filtered)
    print(y_filtered)
    fig4.show()

    #
    x_gold, y_gold, fig_gold = forceVerification.plotForceData(forceVerification.foldername+'testometric/07-04-22_14-11-26.txt')
