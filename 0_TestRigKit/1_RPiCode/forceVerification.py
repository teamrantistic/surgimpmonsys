import arduinoSerialTHRiLS
import dataSaverTHRiLS
import os
import time
import pandas as pd
import plotly
import plotly.express as px
from datetime import datetime

foldername = 'forceVerifData/'

def plotForceData(filename):
    df = pd.read_csv(filename)

    # Drop rows with any empty cells
    df.dropna(
        axis=0,
        how='any',
        thresh=None,
        subset=None,
        inplace=True
    )
    df = df.reset_index(drop=True)

    print(df)

    # df = px.data.iris()  # iris is a pandas DataFrame
    # fig = px.scatter(x=df.index, y=df.iloc[:, 0])
    fig = px.line(x=df.index, y=df.iloc[:, 0])
    print(df.index)
    fig.show()
    return df.index, df.iloc[:,0], fig


if __name__ == '__main__':
    # arduinoSerial = arduinoSerialTHRiLS.connectToArduino('PC_TALBOT', 115200)
    arduinoSerial = arduinoSerialTHRiLS.connectToArduino('laptop', 115200)
    # arduinoSerial = arduinoSerialTHRiLS.connectToArduino('PC', 115200)
   # arduinoResponse = arduinoSerialTHRiLS.readArduinoContinuous(arduinoSerial)
    dataPoint = arduinoSerialTHRiLS.readArduinoContinuous(arduinoSerial) # empty data read
    testType = int(input('select testType\n 1: lm358n config1 \n 2: ad623a board \n 3: lm358n config2\n '))
    if testType == 1:
        testName = "LM358Nconfig1"
    elif testType == 2:
        testName = "AD623Again3k2"
    elif testType == 3:
        testName = "LM358Nconfig2"
    else:
        testName = "unknown"
    dataPoint = arduinoSerialTHRiLS.readArduinoContinuous(arduinoSerial) # empty data read
    filename = foldername + datetime.now().strftime("%d-%m-%y_%H-%M-%S_") + testName + '.csv'
    dataPoint = arduinoSerialTHRiLS.readArduinoContinuous(arduinoSerial) # empty data read
    # for i in range(1, 20000, 1):
    print(filename)
    while True:
        try:
            dataPoint = arduinoSerialTHRiLS.readArduinoContinuous(arduinoSerial)
            # dataPoint = arduinoSerialTHRiLS.readArduinoContinuous(arduinoSerial)
            # arduinoResponse = ser.readline().decode('utf-8').rstrip()
            #print(dataPoint)

            # time.sleep(10)
            # 100mV is 500lbs/force
            # pound force conversion to newtons 1 pound-force is 4.44822 Newton
            if testType == 1:
                amp_gain = 4700.0/220.0 #amplifier gain = R3/R1 = 4.7k/220
            elif testType == 3:
                amp_gain = 4700.0 / 220.0  # amplifier gain = R3/R1 = 4.7k/220
            elif testType == 2:
                amp_gain = ((100000.0/3293.0) + 1.0) #amplifier gain = (100k/Rgain)+1
            else:
                amp_gain = 1
            unamp_volt_scale = 0.1  # 100mV scale unamplified
            amp_volt_scale = unamp_volt_scale * amp_gain  # amplified voltage scale
            scale_newtons = 500.0 * 4.44822 # convert pound force to newtons
            unamp_newtons_per_volt = scale_newtons / unamp_volt_scale
            amp_newtons_per_volt = scale_newtons / amp_volt_scale

            arduino_volt_scale = 3.3
            #amp_voltage_scale = 5.0 #3.3 #volts either 3.3 or 5
            adcBits = 12 #12 bits adc is 2^12 levels which is 4096
            dataPointVoltage = float(dataPoint)*(arduino_volt_scale/(2**adcBits))
            # print(dataPointVoltage)
            dataPointNewtons = dataPointVoltage * amp_newtons_per_volt
            print(dataPointNewtons)

            # dataSaverTHRiLS.saveLiveForceSensorData(str(dataPointVoltage), 'file2.csv')

            dataSaverTHRiLS.saveLiveForceSensorData(str(dataPointNewtons) + ',' + str(dataPointVoltage) + ',' + str(time.time_ns()), filename)
        except KeyboardInterrupt:
            print("exiting data recording ... ")
            break

    plotForceData(filename)

#     df = pd.read_csv(filename)
#
# # Drop rows with any empty cells
#     df.dropna(
#         axis=0,
#         how='any',
#         thresh=None,
#         subset=None,
#         inplace=True
#     )
#     df = df.reset_index(drop=True)
#
#     print(df)
#
#
#
#     #df = px.data.iris()  # iris is a pandas DataFrame
#     fig = px.scatter(x=df.index, y=df.iloc[:, 0])
#     print(df.index)
#     fig.show()