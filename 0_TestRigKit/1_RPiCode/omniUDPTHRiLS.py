import socket
hostname = socket. gethostname() ##should return as raspberrypi
localIP = socket.gethostbyname(hostname + ".local")#"localhost"#"192.168.0.10"#"127.0.0.1" #rpi address

clientIP = socket.gethostbyname("PHD-CRATE" + ".local")
# print(hostname)
# print(localIP)
localPort = 8888
bufferSize = 65536
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

def udp_init():
    # Bind to address and ip
    UDPServerSocket.bind((localIP, localPort))
    print("UDP server up and listening")
    # return [UDPServerSocket,bufferSize]

def udp_send(cli_addr, msgFromServer):
    #msgFromServer = "filename_1234"
    bytesToSend = str.encode(msgFromServer)
    # UDPServerSocket.sendto(bytesToSend, (clientAddress, port))
    UDPServerSocket.sendto(bytesToSend, cli_addr)
    print("Sent:", msgFromServer)

def udp_receive():
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    message = bytesAddressPair[0]
    address = bytesAddressPair[1]
   #clientMsg = "Message from Client:{}".format(message)
    clientMsg = "{}".format(message.decode("utf-8"))
    clientIP = "Client IP Address:{}".format(address)
    return [clientMsg, address]
    #return [message, address]



# msgFromClient = "Hello UDP Server"
#
# bytesToSend = str.encode(msgFromClient)

# serverAddressPort = ("127.0.0.1", localPort)

#
# # Create a UDP socket at client side
# UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
#
# while(1):
#     msgFromServer = UDPClientSocket.recvfrom(bufferSize)
#     msg = "Message from Server {}".format(msgFromServer[0])
#
#     print(msg)

# Create a datagram socket
#
# UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
# # Bind to address and ip
#
# UDPServerSocket.bind((localIP, localPort))


# Sending a reply to client

# clientAddress = socket.gethostbyname("PHD-CRATE" + ".local")
# print(clientAddress)
# msgFromServer = "filename_1234"
# bytesToSend = str.encode(msgFromServer)
# #UDPServerSocket.sendto(bytesToSend, (clientAddress, port))


if __name__ == '__main__':
    count = 0
    udp_init()

    while(True):

        # bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
        #
        # message = bytesAddressPair[0]
        #
        # address = bytesAddressPair[1]

        client_msg, client_addr = udp_receive()
        print(client_msg)#, client_addr)

        if client_msg == "Omni Conn Live":
            if count % 2 == 0:
                #on first data receive
                udp_send(client_addr, "filename_{}".format(count+1))
            else:
                udp_send(client_addr, "filename_{}".format(count))

            client_msg = ""
            count = count + 1



        # print(clientMsg)
        # print(clientIP)

