import math

import cv2
import numpy as np
import pandas as pd
import socket
import struct

import os

from cv2 import aruco
import matplotlib.pyplot as plt

import matplotlib.animation as anim

import plotly.graph_objects as go
#import plotly.express as px
from plotly.subplots import make_subplots

from sklearn.linear_model import LinearRegression

import dataSaverTHRiLS

# Read From eyeCamCalMParams.npz
piCamCalParams = np.load("../../0_calibration/piCamCalParams.npz")
DIM = tuple(piCamCalParams['arr_0'])
K = piCamCalParams['arr_1']
D = piCamCalParams['arr_2']

rvecs = piCamCalParams['arr_3']
tvecs = piCamCalParams['arr_4']

# print("DIM=" + str(DIM))
# print("K=np.array(" + str(K.tolist()) + ")")
# print("D=np.array(" + str(D.tolist()) + ")")

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
parameters = aruco.DetectorParameters_create()

# Create grid board object we're using in our stream
board = aruco.GridBoard_create(
        markersX=2,
        markersY=2,
        markerLength=0.0266,
        markerSeparation=0.0026,
        dictionary=aruco_dict)

#filenames = [img for img in glob.glob("noDroppedImpactionRecordings/30fps/img*.pgm")]
#directory = "noDroppedImpactionRecordings/80fps/"
#directory = "firstSyncWithOmniDataRecorded/"
#directory = "noDroppedImpactionRecordings/80fps1/"
#directory = "noDroppedImpactionRecordings/60fps4markers/"
#directory = "noDroppedImpactionRecordings/arucoboard/"
#directory = "/mnt/ramdisk/"
#expName = "FirstTestWithForce"
#expName = "FirstTestWithForceDiffAngle"
#expName = "FirstTestLowForce"
#expName = "FirstTestVeryHighForce"
expName = "One"
#expName = "Two"
#expName = "Three"
#expName = "Four"
#expName = "Five"
#expName = "Six"
#expName = "Seven"
#expName = "Eight"
#expName = "Nine"
#expName = "Ten"
#directory = "../2_Captures/0_"+expName+"/"
#directory = "../2_Captures/0_RepeatExperimentForce/"+expName+"/"
directory="../../../0_RawCaptures/01_Constant/22_01_12-04_16_47_PM/00/"
directory="../../../0_RawCaptures/01_Constant/22_01_12-05_10_53_PM/03/"
filenames = os.listdir(directory)

filenames.sort() # ADD THIS LINE
print(filenames)

##https://medium.com/@kennethjiang/calibrate-fisheye-lens-using-opencv-333b05afa0b0
def undistort(img_path):
    img = cv2.imread(img_path)
    h,w = img.shape[:2]
    map1, map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), K, DIM, cv2.CV_16SC2)
    undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
    #cv2.imshow("undistorted", undistorted_img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    return undistorted_img

def main():
    #undistort('img00010.pgm')
    noOfDataElements = 4
    hammerPosXYZ = np.zeros(noOfDataElements)    
    introducerPosXYZ = np.zeros(noOfDataElements)    
    frameNo = 0
    #while True:

    rvecs1 = np.zeros([1,1,3])
    tvecs1 = np.zeros([1, 1, 3])
    rvecs1_prev = np.zeros([1,1,3])
    tvecs1_prev = np.zeros([1, 1, 3])

    for file in filenames:
        if file.endswith('.pgm'):
            print(file)
            frameNo = frameNo + 1

            rvecs1_prev = rvecs1#previous frame's homogenous matrix for conversion to marker coords to world coords etc.
            tvecs1_prev = tvecs1

            #gray = cv2.imread(directory+"img00020.pgm", cv2.IMREAD_UNCHANGED)

            #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            ##undistort image
            #print(DIM)
            #final_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(K, D, DIM, np.eye(3), balance=1.0)
            #map1, map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), final_K, DIM, cv2.CV_8S)
            #map1, map2 = cv2.fisheye.initUndistortRectifyMap(K, D, None, K, DIM, 5)
            #frame_undistort = cv2.remap(frame, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
            #frame_undistort = cv2.remap(frame,map1,map2,cv2.INTER_LINEAR)
            #new_K, roi = cv2.getOptimalNewCameraMatrix(K, D, DIM, 1, DIM)
            #frame_undistort = cv2.undistort(frame, K, D, None, K)
            print(directory+file)

            frame_undistort = undistort(directory+file)
            #frame_undistort = undistort(raw_f)
            #frame = cv2.imread(directory+file, cv2.IMREAD_UNCHANGED)
            frame = cv2.imread(directory+file, cv2.IMREAD_UNCHANGED)
            #frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
            #gray = cv2.imread(directory+file, cv2.IMREAD_UNCHANGED)
            #gray = frame_undistort.copy()
            #frame = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
            frame_colour = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)

            #corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
            #corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
            corners, ids, rejectedImgPoints = aruco.detectMarkers(frame_undistort, aruco_dict, parameters=parameters)
           # print(corners[0][0][0][0])

            # Refine detected markers
            # Eliminates markers not part of our board, adds missing markers to the board
            corners, ids, rejectedImgPoints, recoveredIds = aruco.refineDetectedMarkers(
                    image = frame,
                    board = board,
                    detectedCorners = corners,
                    detectedIds = ids,
                    rejectedCorners = rejectedImgPoints,
                    cameraMatrix = K,
                    distCoeffs = D)

            if np.all(ids is not None):
            #if len(ids) == 2:

                rvecs1 = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(len(ids))] # rotation vectors estimated for each cal pattern
                tvecs1 = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(len(ids))] # translation vectors estimated for each cal pattern

                frame_colour = frame_undistort.copy()
                for i in range(0, len(ids)):
                    #rvec, tvec, markerPoints = aruco.estimatePoseSingleMarkers(corners[i], 0.02, K, D)
                    markerLength = 40 # 40mm # 4cm
                    #markerLength = 0.0266 # 40mm # 4cm
                    DistEmpty = np.zeros((4, 1)) # create empty distortion coefficients because we have undistorted image
                    rvecs1[i], tvecs1[i], markerPoints = aruco.estimatePoseSingleMarkers(corners[i], markerLength, K, DistEmpty)
                    (rvecs1[i]-tvecs1[i]).any()

                    hammerHeadLoc = np.array([25.66,-84.85,-5.1,1.0]) #x,y,z in marker coordinate space (realworld) last element 1 for homogenous matrix multiplication
                    introducerHeadLoc = np.array([-61.0,-56.0,-6.0,1.0]) #last element 1 for homogenous matrix multiplication

                    #hammerHeadLoc = np.zeros(3)

                    frame_markers = aruco.drawDetectedMarkers(frame_colour,corners,ids)
                    frame_pose = frame_markers.copy()
                    #frame_pose = aruco.drawAxis(frame_markers, K, DistEmpty, rvecs1[i], tvecs1[i], markerLength/2)


                    if ids[i]==1:

                        #print(rvecs1[i])
                        #print(tvecs1[i])
                    #frame_pose = aruco.drawAxis(frame_colour, K, D, rvec, tvec, markerLength/2)
                    #frame_pose = aruco.drawAxis(frame, K, D, rvec, tvec+tvectrans, 0.01)
                ## we need a homogeneous matrix but OpenCV only gives us a 3x3 rotation matrix
                ##https://answers.ros.org/question/314828/opencv-camera-rvec-tvec-to-ros-world-pose/
                        homog_matrix = np.array([[0, 0, 0, 0],
                               [0, 0, 0, 0],
                               [0, 0, 0, 0],
                               [0, 0, 0, 1]],
                               dtype=float)

                        homog_matrix[:3, :3], _ = cv2.Rodrigues(rvecs1[i])
                        homog_matrix[:3,3] = tvecs1[i] #final column

                        homog_matrix_inv = np.linalg.inv(homog_matrix)

                        homog_matrix_prev = homog_matrix.copy()
                        homog_matrix_prev[:3, :3], _ = cv2.Rodrigues(rvecs1_prev[i])
                        homog_matrix_prev[:3, 3] = tvecs1_prev[i]  # final column

                        #pivotAxisLineLength = 127.65
                        pivotAxisLineLength = 154.65
                        #pivotAxisDepth = 13.48

                        pivotAxisPoint_MSpace = np.array([0.0,pivotAxisLineLength,0.0,1.0]) # in marker space (relative to marker i.e. 0.0 is centre of marker 20 is 20mm away from marker)
                        pivotPoint_WSpace = np.matmul(homog_matrix, pivotAxisPoint_MSpace) #in world space
                        pivotPointPrev_WSpace = np.matmul(homog_matrix_prev,pivotAxisPoint_MSpace)


                        #pivPointReProj = np.matmul(homog_matrix_inv,pivotPointGuess)




                        #print(pivPointReProj)

                        mainMarkerPoint = np.matmul(homog_matrix,np.array([0.0,0.0,0.0,1.0]))

                        newPoint = np.matmul(homog_matrix,hammerHeadLoc) #gives point in aruco marker space
    #                    print(tvecs1[i])
     #                   print(newPoint[:3])
    #                    fig = go.Figure(
    #                        data = [go.Scatter3d(x=newPoint[0], y=newPoint[1], z=newPoint[2], mode='markers')]
    #                    )
    #                    fig.show()


                        newPoint[3] = frameNo # set last element to frame number instead of just 1
                        hammerPosXYZ = np.append(hammerPosXYZ, newPoint)
                        frame_pose = aruco.drawAxis(frame_pose, K, DistEmpty, rvecs1[i], newPoint[:3],markerLength/2)
                        #frame_pose = aruco.drawAxis(frame_pose, K, DistEmpty, rvecs1[i], pivotPoint_WSpace[:3], markerLength/2)
                        #pivotAxis = np.float32([[0,200.5,0]])
                        pivotAxis = np.float32([[0,0,0], [0,pivotAxisLineLength,0]]).reshape(-1,3)
                        #axis = np.float32([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
                        #                     [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]]).reshape(-1, 3)
                        #print(axis)
                        #print(pivotPointGuess)
                        pivotAxisImgPoints, jac = cv2.projectPoints(pivotAxis, rvecs1[i], tvecs1[i], K, DistEmpty)
                        pivotAxisImgPoints_prev, jac = cv2.projectPoints(pivotAxis, rvecs1_prev[i], tvecs1_prev[i], K, DistEmpty)

                        #print(imgpts[0][0])
                        #imgpointx, imgpointy = corners[0][0][0]
                        #pivotAxisStartX, pivotAxisStartY = pivotAxisImgPoints[0][0]
                        #pivotAxisEndX, pivotAxisEndY = pivotAxisImgPoints[1][0]
                        #frame_pose = cv2.line(frame_pose, (pivotAxisStartX, pivotAxisStartY), (pivotAxisEndX, pivotAxisEndY), (0, 255, 0), thickness=2)
                        #pivotAxisStartX_prev, pivotAxisStartY_prev = pivotAxisImgPoints_prev[0][0]
                       # pivotAxisEndX_prev, pivotAxisEndY_prev = pivotAxisImgPoints_prev[1][0]
                        #frame_pose = cv2.line(frame_pose, (pivotAxisStartX_prev, pivotAxisStartY_prev),  (pivotAxisEndX_prev, pivotAxisEndY_prev), (0, 0, 255), thickness=2)

                        ##calculate angle between two lines:
                        #vector_1=np.subtract(pivotAxisImgPoints_prev[1],pivotAxisImgPoints_prev[0])
                        #vector_2=np.subtract(pivotAxisImgPoints[1],pivotAxisImgPoints[0])

                        # vector_1 = pivotPointPrev_WSpace
                        # vector_2 = pivotPoint_WSpace
                        #
                        # #unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
                        # #unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
                        # #dot_product = np.dot(unit_vector_1, unit_vector_2)
                        # dot_product = np.dot(vector_1, vector_2)
                        # angle = np.arccos(dot_product/(np.linalg.norm(vector_1)*np.linalg.norm(vector_2)))
                        # angleDeg =math.degrees(angle)
                        # print('Angle:')
                        # print(angleDeg)
                        # print(angle)


    #             pose, rvec, tvec = aruco.estimatePoseBoard(corners, ids, board, K, D, rvecs1, tvecs1)
    #             if pose:
    #                 # Draw the camera posture calculated from the gridboard
    #                 QueryImg = aruco.drawAxis(frame_colour.copy(), K, D, rvec, tvec, markerLength/2)
    #                 cv2.imshow('frame_board', QueryImg )
                    #cv2.waitKey(0)
                    #frame_markers = aruco.drawDetectedMarkers(frame_pose.copy(),corners,ids)

                    elif ids[i] == 7:
                        homog_matrix = np.array([[0, 0, 0, 0],
                               [0, 0, 0, 0],
                               [0, 0, 0, 0],
                               [0, 0, 0, 1]],
                               dtype=float)
                        homog_matrix[:3, :3], _ = cv2.Rodrigues(rvecs1[i])
                        homog_matrix[:3,3] = tvecs1[i] #final column

                        newPoint = np.matmul(homog_matrix,introducerHeadLoc)
      #                  print(tvecs1[i])
       #                 print(newPoint[:3])
                        newPoint[3] = frameNo
                        introducerPosXYZ = np.append(introducerPosXYZ, newPoint)

                        frame_pose = aruco.drawAxis(frame_pose, K, DistEmpty, rvecs1[i], newPoint[:3], markerLength/2)

                cv2.imshow('frame', frame_pose)
                #cv2.imshow('frame_original', frame)
                #cv2.imshow('frame_board', QueryImg )
                #cv2.waitKey(0)
                    #if cv2.waitKey(1) & 0xFF == ord('q'):
                     #   break
                #else:
                #    print(file)
        hammerPosXYZ = np.reshape(hammerPosXYZ, (-1,noOfDataElements))
        hammerPosXYZ = np.delete(hammerPosXYZ, (0), axis=0) #delete first row of zeros
        introducerPosXYZ = np.reshape(introducerPosXYZ, (-1,noOfDataElements))
        introducerPosXYZ = np.delete(introducerPosXYZ, (0), axis=0) #delete first row of zeros

        hammerVelXYZ = np.diff(hammerPosXYZ,axis=0)
        hammerVelXYZ[:,3] = hammerVelXYZ[:,3]/100.0 #convert last column(difference in frames) to time by dividing by fps
    #    hammerVel[:,:2] = hammerVelXYZ[:,:2]/hammerVelXYZ[:,3]
        hammerAccXYZ = np.diff(hammerVelXYZ,axis=0)
        hammerAccXYZ[:,3] = hammerVelXYZ[:-1,3] #(:-2 selects all up to second to last row)last columns is replaced with same time values as velocity
        #print (hammerPosXYZ)
        #print (hammerVelXYZ)
        #print (hammerAccXYZ)
    #    print (introducerPosXYZ)
    #    trace1 = {go.Scatter3d(x=hammerPosXYZ[:,0]*-1, z=hammerPosXYZ[:,1]*-1, y=hammerPosXYZ[:,2], mode='markers')}
    #trace2 = {go.Scatter3d(x=introducerPosXYZ[:,0]*-1, z=introducerPosXYZ[:,1]*-1, y=introducerPosXYZ[:,2], mode='markers')}

        fig = go.Figure(
            data = [go.Scatter3d(
                x=hammerPosXYZ[:,0]*-1,
                z=hammerPosXYZ[:,1]*-1,
                y=hammerPosXYZ[:,2],
                mode='markers',
                marker= dict(
                size = 3,
                color=hammerPosXYZ[:,3],
                colorscale='YlGnBu',
                opacity=0.8
                )
                ), go.Scatter3d(
                x=introducerPosXYZ[:,0]*-1,
                z=introducerPosXYZ[:,1]*-1,
                y=introducerPosXYZ[:,2],
                mode='markers',
                marker= dict(
                size = 3,
                color=hammerPosXYZ[:,3],
                colorscale='YlOrBr',
                opacity=0.8
                )
                )]



        )

        #fig.show()

        fig2 = go.Figure()
        fig2 = make_subplots(rows = 4, cols = 1, shared_xaxes=False)#True)

        fig2.add_trace(go.Scatter(x=hammerPosXYZ[:,3], y=hammerPosXYZ[:,0], name='XPos',mode='lines+markers' ),row=1,col=1)
    #    fig2.add_trace(go.Scatter(x=hammerPosXYZ[:,3], y=hammerVelXYZ[:,0], name='Xvel',mode='lines+markers' ))
        fig2.add_trace(go.Scatter(x=hammerPosXYZ[:,3], y=hammerVelXYZ[:,0]/hammerVelXYZ[:,3], name='Xvel',mode='lines+markers' ),row=2,col=1)
        fig2.add_trace(go.Scatter(x=hammerPosXYZ[:,3], y=hammerAccXYZ[:,0]/hammerVelXYZ[:-1,3]**2, name='Xacc',mode='lines+markers' ),row=3,col=1)
        #forceData = pd.read_csv("../2_Captures/"+expName+".csv", dtype=int)
        forceData = pd.read_csv(directory+"ForceSensorData.csv")
        fig2.add_trace(go.Scatter(x=forceData.iloc[:,0], y=forceData.iloc[:,1], name='ForceSensor',
                                  mode='lines+markers'), row=4, col=1)
        #print(forceData)
    #fig2.add_trace()
#    fig2.add_trace(go.Scatter(x=hammerPosXYZ[:,3], y=hammerVelXYZ[:,1]/hammerVelXYZ[:,3], name='Yvel',mode='lines+markers' ))
#    fig2.add_trace(go.Scatter(x=hammerPosXYZ[:,3], y=hammerVelXYZ[:,2]/hammerVelXYZ[:,3], name='Zvel',mode='lines+markers' ))
        fig2.show()
cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
