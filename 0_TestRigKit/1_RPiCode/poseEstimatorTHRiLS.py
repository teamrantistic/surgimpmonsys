import cv2
import numpy as np
import pandas as pd
import socket
import struct

import os

from cv2 import aruco
import math
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from sklearn.linear_model import LinearRegression

import dataSaverTHRiLS

# Read From eyeCamCalMParams.npz
piCamCalParams = np.load("../../0_calibration/piCamCalParams.npz")
DIM = tuple(piCamCalParams['arr_0'])
K = piCamCalParams['arr_1']
D = piCamCalParams['arr_2']

rvecs = piCamCalParams['arr_3']
tvecs = piCamCalParams['arr_4']

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
parameters = aruco.DetectorParameters_create()

# # Create grid board object we're using in our stream
# board = aruco.GridBoard_create(
#         markersX=2,
#         markersY=2,
#         markerLength=0.0266,
#         markerSeparation=0.0026,
#         dictionary=aruco_dict)

directory="../../../0_RawCaptures/01_Constant/22_01_12-04_16_47_PM/00/"
directory="../../../0_RawCaptures/01_Constant/22_01_12-08_25_51_PM/00/"
directory="../../../0_RawCaptures/fixedArucoMarkers/"
directory = "../../../0_RawCaptures/01_Constant/22_02_02-03_05_18_PM/01/"
directory = "../../../0_RawCaptures/01_Constant/22_02_02-03_30_19_PM/00/"
directory = "../../../0_RawCaptures/01_Constant/22_02_02-05_00_41_PM/00/"
directory = "../../../0_RawCaptures/01_Constant/22_02_02-07_33_31_PM/00/"
directory = "../../../0_RawCaptures/01_Constant/22_02_08-06_31_10_PM/00/"
filenames = os.listdir(directory)
filenames.sort() # ADD THIS LINE
print(filenames)

debug = True

##https://medium.com/@kennethjiang/calibrate-fisheye-lens-using-opencv-333b05afa0b0
def undistort(img_path):
    img = cv2.imread(img_path)
    h,w = img.shape[:2]
    #map1, map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), K, DIM, cv2.CV_16SC2)
    map1, map2 = cv2.initUndistortRectifyMap(K, D, np.eye(3), K, DIM, cv2.CV_16SC2)
    undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
    # cv2.imshow("undistorted", undistorted_img)
    # cv2.waitKey(0)
    #cv2.destroyAllWindows()
    return undistorted_img

##https://learnopencv.com/rotation-matrix-to-euler-angles/
# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R):
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype=R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6


# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R):
    assert (isRotationMatrix(R))

    sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])

    singular = sy < 1e-6

    if not singular:
        x = math.atan2(R[2, 1], R[2, 2])
        y = math.atan2(-R[2, 0], sy)
        z = math.atan2(R[1, 0], R[0, 0])
    else:
        x = math.atan2(-R[1, 2], R[1, 1])
        y = math.atan2(-R[2, 0], sy)
        z = 0

    return np.array([x, y, z])


def markerPosInfo(apparatusName, markerType, ids, corners_Ispace):
    apparatusId = 0
    markerLength = 0
    if ids is not None:
        if markerType == "sing": #if single marker
            if apparatusName == "mallet":
                apparatusId = 1

            elif apparatusName == "pivot":
                apparatusId = 5

            elif apparatusName == "introducer":
                apparatusId = 9

            index = np.where(ids == apparatusId)
            index = index[0][0]

        elif markerType == "quad": #if set of 4 markers
            if apparatusName == "mallet":
                apparatusId = [1,2,3,4]
                ##calc transform of id
                ##if apparatus id = 1 (top left) transform to pivot centre = transform to marker centre + hammer handle length

            elif apparatusName == "pivot":
                apparatusId = [5,6,7,8]
            elif apparatusName == "introducer":
                apparatusId = [9, 10, 11, 12]

            #print('appId:',apparatusId)
            markerLength = 15.03  # mm
            index = []
            for app_id in apparatusId:
                indexSingle = np.where(ids == app_id)
                index.append(indexSingle[0][0])
                print('indices:',index)

            index = np.array(index) # cannot select multiple indices using list, need to convert list to numpy array

            print('arrayindices: ',index)

        print('idsselectedwithlist: ',ids[index])
        print('cornersselectedwithlist: ', corners_Ispace[index[0]])

        return [ids[index], corners_Ispace[index[0]]]
    else:
        return [-1, -1]

    # elif markerType == "quad":
    #     if markerID == 1: #malletType



if __name__ == "__main__":
    noOfDataElements = 4
    hammerPosXYZ = np.zeros(noOfDataElements)
    introducerPosXYZ = np.zeros(noOfDataElements)
    frameNo = 0
    rvecs1 = np.zeros([1,1,3])
    tvecs1 = np.zeros([1, 1, 3])

    distance_error_cumulative = 0
    distance_error_cumulative_undist = 0
    count = 0

    # real_distance = 121.62#
    #real_distance = 120.65
    real_distance = 123.65


    for file in filenames:
        if file.endswith('.pgm'): #if file is an image
            frame_undistorted = undistort(directory + file) # undistort img and store as frame_undistorted
            #frame_undistorted = cv2.fromarray(frame_undistorted)

            frame = cv2.imread(directory+file, cv2.IMREAD_UNCHANGED)
            #frame = cv2.imread(directory+file, cv2.IMREAD_GRAYSCALE)


            # #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # blur = cv2.GaussianBlur(frame, (5, 5), 0)
            # ret1, bnw = cv2.threshold(blur, 0, 100,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            # #ret1, bnw_undist = cv2.threshold(frame_undistorted, 0, 100, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            # frame = bnw
            # #cv2.imshow('gray',gray)
            # cv2.imshow('frame', frame)
            # cv2.waitKey(0)
            # if debug==True:
            #    # print('cornerCoords in image space'+corners_Ispace)#corners are given in image coordinates
            #    cv2.imshow('distorted', frame)
            #    cv2.imshow('undistorted', frame_undistorted)
            #    cv2.waitKey(0)

            corners_Ispace, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict,cameraMatrix= K,distCoeff=D)  # detect ,markers
            corners_Ispace_undist, ids_undist, rejectedImgPoints_undist = aruco.detectMarkers(frame_undistorted,
                                                                                              aruco_dict)  # detect ,markers
            print(corners_Ispace)
            #print(markerPosInfo("mallet", "sing", ids, corners_Ispace))
            ids_introducer, corners_Ispace_introducer = markerPosInfo("introducer", "quad", ids, corners_Ispace)
            #print("id:",ids," corners: ", corners_Ispace)
            print("id:", ids_introducer, " corners: ", corners_Ispace_introducer)

            # # Refine detected markers
            # # Eliminates markers not part of our board, adds missing markers to the board
            # corners_Ispace, ids, rejectedImgPoints, recoveredIds = aruco.refineDetectedMarkers(
            #         image = frame,
            #         board = board,
            #         detectedCorners = corners_Ispace,
            #         detectedIds = ids,
            #         rejectedCorners = rejectedImgPoints,
            #         cameraMatrix = K,
            #         distCoeffs = D)
            # # Refine detected markers
            # # Eliminates markers not part of our board, adds missing markers to the board
            # corners_Ispace_undist, ids_undist, rejectedImgPoints_undist, recoveredIds_undist = aruco.refineDetectedMarkers(
            #         image = frame_undistorted,
            #         board = board,
            #         detectedCorners = corners_Ispace_undist,
            #         detectedIds = ids_undist,
            #         rejectedCorners = rejectedImgPoints_undist
            # )

            frame = aruco.drawDetectedMarkers(frame, corners_Ispace, ids, (0, 0, 255))
            frame_undistorted = aruco.drawDetectedMarkers(frame_undistorted, corners_Ispace_undist, ids_undist, (0, 0, 255))
            #frame = aruco.drawDetectedMarkers(frame, corners_Ispace, (0, 0, 255))
            # frame_undistorted = aruco.drawDetectedMarkers(frame_undistorted, corners_Ispace_undist, (0, 0, 255))
            cv2.imshow('distorted', frame)
            cv2.imshow('undistorted', frame_undistorted)
            cv2.waitKey(0)
            #
            # print(ids)
            # print(ids_undist)
            # cv2.destroyAllWindows()
            #D = np.array([-0.46072341,0.37730898,0.00176783,0.00102155, -0.42895404])
            #print(D)


            if ids is not None and ids_undist is not None:
                if len(ids_undist) == 2 and len(ids) == 2:
                    count = count + 1

                #if np.all(ids is not None) and len(ids) == 2:
                    rvecs1 = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(len(ids))]  # empty array to store rotation vector
                    tvecs1 = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(len(ids))]  # empty array to store translation vector

                    rvecs1_undist = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(len(ids))]  # empty array to store rotation vector
                    tvecs1_undist = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(len(ids))]  # empty array to store translation vector

                    for i in range(0, len(ids_undist)):
                        #rvec, tvec, markerPoints = aruco.estimatePoseSingleMarkers(corners[i], 0.02, K, D)
                        if ids[i] == 1:
                            markerLength = 40.79 # 40mm # 4cm
                        else:
                            markerLength = 40.22
                        # markerLength = 38.15 #mm
                        DistEmpty = np.zeros((4, 1)) # create empty distortion coefficients because we have undistorted image
                        #DistEmpty=D
                        rvecs1[i], tvecs1[i], corners_Mspace = aruco.estimatePoseSingleMarkers(corners_Ispace[i], markerLength, K, D)
                        (rvecs1[i]-tvecs1[i]).any()
                        rvecs1_undist[i], tvecs1_undist[i], corners_Mspace_undist = aruco.estimatePoseSingleMarkers(corners_Ispace_undist[i], markerLength, K, DistEmpty)
                        (rvecs1[i]-tvecs1[i]).any()

                        # print("rvecs:", rvecs1[i])
                        # print("tvecs: ", tvecs1[i])
                        # print("rvecs_undist: ", rvecs1_undist[i])
                        # print("tvecs_undist: ", tvecs1_undist[i])

                        homog_matrix = np.array([[0, 0, 0, 0],
                                                 [0, 0, 0, 0],
                                                 [0, 0, 0, 0],
                                                 [0, 0, 0, 1]], dtype=float)
                        homog_matrix[:3, :3], _ = cv2.Rodrigues(rvecs1[i][0][0])
                        homog_matrix[:3, 3] = tvecs1[i]  # final column

                        homog_matrix_undist = np.array([[0, 0, 0, 0],
                                                 [0, 0, 0, 0],
                                                 [0, 0, 0, 0],
                                                 [0, 0, 0, 1]], dtype=float)
                        homog_matrix_undist[:3, :3], _ = cv2.Rodrigues(rvecs1_undist[i][0][0])
                        homog_matrix_undist[:3, 3] = tvecs1_undist[i]  # final column

                        # print("homog_matrix:\n", homog_matrix)
                        # print("homog_matrix_undist:\n", homog_matrix_undist)

                        #point_Mspace = np.array([-185.0,-100.0,0.0,1.0])
                        z = 0#26
                        if ids_undist[i] == 1 or ids_undist[i] == 2:
                            # point_Mspace = np.array([0.0, -121.62, z])
                            # point_Mspace_undist = np.array([0.0, -121.62, z])
                            point_Mspace = np.array([-5.0,real_distance, z])
                            point_Mspace_undist = np.array([-5.0, real_distance, z])
                            # point_Mspace = np.array([0.0, 0.0, z])
                            # point_Mspace_undist = np.array([0.0, 0.0, z])
                            colour = (0, 0, 255) #red
                        else:
                            # point_Mspace = np.array([0.0, 121.62, z*-1.0])
                            # point_Mspace_undist = np.array([0.0, 121.62, z*-1.0])
                            point_Mspace = np.array([0.0, 0.0, z])
                            point_Mspace_undist = np.array([0.0, 0.0, z])
                            colour = (0, 255, 0) #green
                        #point_Mspace = np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)
                        #axis = np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)
                        #point_Ispace, jac = cv2.projectPoints(axis, rvecs1[i],tvecs1[i],K,D)#np.matmul(homog_matrix,point_Mspace)
                        point_Ispace, jac = cv2.projectPoints(point_Mspace, rvecs1[i], tvecs1[i], K,
                                                              D)  # np.matmul(homog_matrix,point_Mspace)
                        point_Ispace_undist, jac_undist = cv2.projectPoints(point_Mspace_undist, rvecs1_undist[i], tvecs1_undist[i], K,DistEmpty)  # np.matmul(homog_matrix,point_Mspace)

                        # print("point_Ispace: ", point_Ispace)
                        # print("point_Ispace_undist: ", point_Ispace_undist)



                        frame = cv2.circle(frame, (int(point_Ispace[0,0][0]), int(point_Ispace[0,0][1])), 3, colour, 1)
                        frame_undistorted = cv2.circle(frame_undistorted, (int(point_Ispace_undist[0, 0][0]), int(point_Ispace_undist[0, 0][1])), 3, colour, 1)

                        frame_undistorted = aruco.drawDetectedMarkers(image=frame_undistorted, corners=corners_Ispace_undist, ids=ids_undist)

                        cv2.imshow('distorted', frame)
                        cv2.imshow('undistorted', frame_undistorted)
                        cv2.waitKey(0)

                    distance = math.sqrt ((tvecs1[0][0][0][0] - tvecs1[1][0][0][0])**2 + (tvecs1[0][0][0][1] - tvecs1[1][0][0][1])**2 + (tvecs1[0][0][0][2] - tvecs1[1][0][0][2])**2)
                    distance_undist = math.sqrt((tvecs1_undist[0][0][0][0] - tvecs1_undist[1][0][0][0]) ** 2 + (tvecs1_undist[0][0][0][1] - tvecs1_undist[1][0][0][1]) ** 2 + (tvecs1_undist[0][0][0][2] - tvecs1_undist[1][0][0][2]) ** 2)

                    distance_error = real_distance - distance
                    distance_error_undist = real_distance - distance_undist
                    #  print('distance: ', distance, ' mm \t distance_undist: ', distance_undist, ' mm')
                    print('distance_error: ', distance_error, ' mm \t   distance_error_undist: ', distance_error_undist, ' mm')

                    distance_error_cumulative = distance_error_cumulative + abs(distance_error)
                    distance_error_cumulative_undist = distance_error_cumulative_undist + abs(distance_error_undist)
                else:
                    print("only 1 marker detected in: ",file)
            else:
                print("no markers detecting in: ", file)
    if count > 0:

        print('avr_distance_error: ', distance_error_cumulative/count, ' mm \t  avr_distance_error_undist: ', distance_error_cumulative_undist / count, ' mm')
        print("count = ", count)



                    # #print(corners_Mspace[0,0][0])
                    #
                    # #corner_Mspace_4D = np.array([corners_Mspace[0,0][0], corners_Mspace[0,0][1], corners_Mspace[0,0][2], 1.0])
                    # corner_Mspace_4D = np.array([corners_Mspace[0, 0][0], 127.0, corners_Mspace[0, 0][2], 1.0])
                    # corner_Ispace_4D = np.array([corners_Ispace[i][0,0][0], corners_Ispace[i][0,0][1], 0.0, 1.0])
                    #
                    # #corner2_Mspace_4D = np.array([corners_Mspace[1, 0][0], corners_Mspace[1, 0][1], corners_Mspace[1, 0][2], 1.0])
                    # corner2_Mspace_4D = np.array([0.0, 0.0, 0.0, 1.0])
                    # # if ids[i] == 1:
                    # #     corner2_Mspace_4D = np.array([0.0, 127.0, 0.0, 1.0])
                    #
                    #
                    # corner2_Ispace_4D = np.array([corners_Ispace[i][0, 1][0], corners_Ispace[i][0, 1][1], 0.0, 1.0])
                    #
                    # homog_matrix_inv = np.linalg.inv(homog_matrix)
                    # #newPoint = np.matmul(homog_matrix_inv, corner_Mspace_4D)
                    # #newPoint = np.matmul(homog_matrix, corner_Ispace_4D)
                    # #homog_matrix_inv = np.matmul(K,homog_matrix_inv)
                    #
                    #
                    # corner_Ispace_Reproj,_ = cv2.projectPoints(corner_Mspace_4D[:3], rvecs1[i], tvecs1[i], K, DistEmpty) #markerspace to image space
                    # corner2_Ispace_Reproj, _ = cv2.projectPoints(corner2_Mspace_4D[:3], rvecs1[i], tvecs1[i], K, DistEmpty)  # markerspace to image space
                    # if ids[i] == 1:
                    #     x1, y1 = corner2_Ispace_Reproj[0,0]
                    # else:
                    #     x2, y2 = corner2_Ispace_Reproj[0,0]
                    #     euc_dist = math.sqrt(((x1-x2)**2)+((y1-y2)**2))
                    #     print(euc_dist)
                    # #print(K)
                    # #corner_Ispace_Reproj = int(corner_Ispace_Reproj)
                    #
                    # corner_Wspace = np.matmul(homog_matrix_inv,
                    #                           corner_Ispace_4D)  # image space to world space transform
                    # corner2_Wspace = np.matmul(homog_matrix_inv,
                    #                            corner2_Ispace_4D)  # image space to world space transform
                    #
                    #
                    # eulers = rotationMatrixToEulerAngles(homog_matrix[:3, :3])
                    # for angle in eulers:
                    #     angle = math.degrees(angle)
                    #     #print(angle)
                    #
                    # if debug == True:
                    #    # print("markerPoints:" + str(corners_Mspace))
                    #    # print(rvecs1[i])
                    #    #  print(corner_Mspace_4D)
                    #    #  #print(corner2_Mspace_4D)
                    #    #  print(corner_Ispace_Reproj[0, 0])
                    #    #  print(corner2_Ispace_Reproj[0, 0])
                    #     #print ('worldspace'+ str(ids[i]))
                    #     #print(corner_Wspace)
                    #     #print('worldspace2')
                    #     #print(corner2_Wspace)
                    #     #print(eulers)
                    #     #print(rvecs1[i][0][0])
                    #     #print(str(corner_Ispace_Reproj[0,0][1] - corner2_Ispace_Reproj[0,0][1]))
                    #     #print(euc_dist)
                    #     #frame = cv2.circle(frame_undistorted,(int(corner_Ispace_Reproj[0,0][0]),int(corner_Ispace_Reproj[0,0][1])),3,(0,255,0),1)
                    #     #frame = aruco.drawDetectedMarkers(frame_undistorted, corners_Ispace, ids, (0, 0, 255))
                    #     frame = aruco.drawDetectedMarkers(frame, corners_Ispace, ids, (0, 0, 255))
                    #     frame = cv2.circle(frame, (int(corner2_Ispace_Reproj[0, 0][0]), int(corner2_Ispace_Reproj[0, 0][1])), 3, (0, 0, 255), 1)
                    #     cv2.imshow('frame dot',frame)
                    #     #print(corner_Wspace)
                    #
