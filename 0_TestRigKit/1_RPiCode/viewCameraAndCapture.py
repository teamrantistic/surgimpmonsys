import time
import os
import subprocess
import re

cameraModeDirectory = "/home/pi/cam-mipiov9281/Linux_4.19.118/ov9281/pi4/"

def set_camera_mode(mode):
    #preview is 9(640x400), capture is 5 (1280x720)
    #Mode       ResRatio        DataFormat      FrameRate
    #0          1280x800        Y10             120fps
    #1          1280x800        Y8              144fps
    #2          1280x800        Y10             EXT_TRIG
    #3          1280x800        Y8              EXT_TRIG
    #4          1280x720        Y10             120fps
    #5          1280x720        Y8              144fps
    #6          1280x720        Y10             EXT_TRIG
    #7          1280x720        Y8              EXT_TRIG
    #8          640x400         Y10             210fps
    #9          640x400         Y8              253fps
    #10         640x400         Y10             EXT_TRIG
    #11         640x400         Y8              EXT_TRIG

    get_mode_process = subprocess.Popen(["cat", "/sys/module/vc_mipi_ov9281/parameters/sensor_mode"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    curr_sensor_mode = get_mode_process.communicate()
    if curr_sensor_mode != mode:
        curr_dir = os.getcwd()#get current directory
        os.chdir(cameraModeDirectory)#change directory to cameraModedirecotroy
        set_mode_process = subprocess.Popen(["make", "setmode"+str(mode)])#run 'make setmode0'
        set_mode_process.wait()#wait for output to go
        os.chdir(curr_dir)#return to original directory

def main():
    result = os.chdir("/mnt/ramdisk")
    if result != None:
        print("could not execute successfully error: ", result)
        exit()

    folder_contents = subprocess.run(["ls"], capture_output=True, text=True)

    if re.search("vcmipidemo", folder_contents.stdout):
        print("vcmipidemo already exists...skipping copy")
    else:
        copy_result = subprocess.run(
            ["cp", "/home/pi/surgimpmonsys/0_TestRigKit/1_RPiCode/vcmipidemo",
             "/mnt/ramdisk/"])
        print("copy errors? : %d" % copy_result.returncode)
        if copy_result.returncode != 0:
            print("could not copy! .. exiting with error: ", copy_result)
            exit()

    n = 0

    while (1):
        set_camera_mode(9)
        gain = '0x88'
        shutter = '872100'
        preview_subprocess = subprocess.Popen(["./vcmipidemo", "-s", shutter, "-g", gain, "-f", ">/dev/null", "-a"],
                                              stdout=subprocess.PIPE)

        user_input = input("capture? y (or enter)/n:")
        preview_subprocess.terminate()
        while (preview_subprocess.poll() == None):  # if process did not terminate
            # print('could not terminate preview process, trying again...')
            preview_subprocess.terminate()
        if user_input == 'n':
            exit()
        else:
            ##clear previous files
            list_of_files = os.listdir("/mnt/ramdisk")
            for frame in list_of_files:
                if frame.endswith(".pgm") or frame.endswith(".csv"):
                    os.remove(os.path.join("/mnt/ramdisk/", frame))

            no_of_caps = len(os.listdir('/mnt/ramdisk'))
            set_camera_mode(5)
            print('preview process terminated, starting capture process...')
            capture_subprocess = subprocess.Popen(
                ["./vcmipidemo", "-s", shutter, "-g", gain, "-f", ">/dev/null", "-o", "-a"], stdout=subprocess.PIPE)
            # capture_subprocess.wait()#openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", ">/dev/null"], stdout=None,                                       #stderr=None)
            # print('captured\n')

            while (len(os.listdir('/mnt/ramdisk')) <= no_of_caps + 1):  ## while no captures do nothing
                print('capturing...')

            capture_subprocess.terminate()
            while (capture_subprocess.poll() == None):  # if process did not terminate
                capture_subprocess.terminate()
            print('capture process terminated')

            files = sorted(os.listdir('/mnt/ramdisk'))
            os.remove(files[
                          len(files) - 2])  # because vcmipidemo file will always be last on the list, delete second to last file instead
            files2 = sorted(os.listdir('/mnt/ramdisk'))
            print('shot captured!')
            print(files2[len(files2) - 2])

            if len(files2) > 1:
                copy_result = subprocess.run(
                    'cp /mnt/ramdisk/*.pgm /home/pi/surgimpmonsys/0_calibration/4_checkerboard/' + str(n).zfill(
                        2) + '.pgm', shell=True)
                print("copy errors? : %d" % copy_result.returncode)
                if copy_result.returncode != 0:
                    print("could not copy images! .. exiting with error: ", copy_result)

            n = n + 1

if __name__ == '__main__':
    main()
    # result = os.chdir("/mnt/ramdisk")
    # if result != None:
    #     print("could not execute successfully error: ", result)
    #     exit()
    #
    # folder_contents = subprocess.run(["ls"], capture_output=True, text=True)
    #
    # if re.search("vcmipidemo", folder_contents.stdout):
    #     print("vcmipidemo already exists...skipping copy")
    # else:
    #     copy_result = subprocess.run(
    #         ["cp", "/home/pi/surgimpmonsys/0_TestRigKit/1_RPiCode/vcmipidemo",
    #          "/mnt/ramdisk/"])
    #     print("copy errors? : %d" % copy_result.returncode)
    #     if copy_result.returncode != 0:
    #         print("could not copy! .. exiting with error: ", copy_result)
    #         exit()
    #
    #
    #
    # n = 0
    #
    # while(1):
    #     set_camera_mode(9)
    #     gain = '0x88'
    #     shutter = '872100'
    #     preview_subprocess = subprocess.Popen(["./vcmipidemo", "-s", shutter, "-g", gain, "-f", ">/dev/null", "-a"], stdout=subprocess.PIPE)
    #
    #     user_input = input("capture? y (or enter)/n:")
    #     preview_subprocess.terminate()
    #     while (preview_subprocess.poll() == None):  # if process did not terminate
    #         # print('could not terminate preview process, trying again...')
    #         preview_subprocess.terminate()
    #     if user_input == 'n':
    #         exit()
    #     else:
    #         ##clear previous files
    #         list_of_files = os.listdir("/mnt/ramdisk")
    #         for frame in list_of_files:
    #             if frame.endswith(".pgm") or frame.endswith(".csv"):
    #                 os.remove(os.path.join("/mnt/ramdisk/", frame))
    #
    #         no_of_caps = len(os.listdir('/mnt/ramdisk'))
    #         set_camera_mode(5)
    #         print('preview process terminated, starting capture process...')
    #         capture_subprocess = subprocess.Popen(
    #             ["./vcmipidemo", "-s", shutter, "-g", gain, "-f", ">/dev/null", "-o", "-a"], stdout=subprocess.PIPE)
    #         # capture_subprocess.wait()#openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", shut, "-g", gai, "-f", ">/dev/null"], stdout=None,                                       #stderr=None)
    #         # print('captured\n')
    #
    #         while (len(os.listdir('/mnt/ramdisk')) <= no_of_caps + 1):  ## while no captures do nothing
    #             print('capturing...')
    #
    #         capture_subprocess.terminate()
    #         while (capture_subprocess.poll() == None):  # if process did not terminate
    #             capture_subprocess.terminate()
    #         print('capture process terminated')
    #
    #         files = sorted(os.listdir('/mnt/ramdisk'))
    #         os.remove(files[len(files) - 2])  # because vcmipidemo file will always be last on the list, delete second to last file instead
    #         files2 = sorted(os.listdir('/mnt/ramdisk'))
    #         print('shot captured!')
    #         print(files2[len(files2) - 2])
    #
    #         if len(files2) > 1:
    #             copy_result = subprocess.run(
    #                 'cp /mnt/ramdisk/*.pgm /home/pi/surgimpmonsys/0_calibration/4_checkerboard/' + str(n).zfill(
    #                     2) + '.pgm', shell=True)
    #             print("copy errors? : %d" % copy_result.returncode)
    #             if copy_result.returncode != 0:
    #                 print("could not copy images! .. exiting with error: ", copy_result)
    #
    #         n = n+1

