import cv2
#assert cv2.__version__[0] == '3', 'The fisheye module requires opencv version >= 3.0.0'

import numpy as np
import os
import glob

import datetime
import pytz

#function to get current time for timestamping images
def utcnow():
    return datetime.datetime.now(tz=pytz.utc)

#width = 6
#height = 9
squaremm = 25#10 = 1cm
width = 6#5
height = 9#8
CHECKERBOARD = (width,height)

#for interpolation obetween pixels
subpix_criteria = (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1)

calibration_flags = cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC+cv2.fisheye.CALIB_CHECK_COND+cv2.fisheye.CALIB_FIX_SKEW

#initialising object points in the checkerboard coordinate frame
objp = np.zeros((1, CHECKERBOARD[0]*CHECKERBOARD[1], 3), np.float64)
objp[0,:,:2] = (np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2))*squaremm

print(objp)
#objp = np.array([objp])

#objpoints = np.array([objpoints])

_img_shape = None
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.


images = glob.glob('3_checkerboard/*.pgm')
n = 0
for fname in images:

    img = cv2.imread(fname)
    if _img_shape == None:
        _img_shape = img.shape[:2]
    else:
        assert _img_shape == img.shape[:2], "All images must share the same size."

    # Convert image to opencv grayscale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_FAST_CHECK+cv2.CALIB_CB_NORMALIZE_IMAGE)


    #corners = np.array([[corner for [corner] in corners]])# https: // stackoverflow.com / questions / 49100256 / python - cv2 - calibratecamera - throws - error - objectpoints - should - contain - vector - of - v
    corners = corners.reshape(corners.shape[0], corners.shape[2])

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
        cv2.cornerSubPix(gray,corners,(3,3),(-1,-1),subpix_criteria)

        imgpoints.append(corners) 
        cv2.drawChessboardCorners(img,(width,height), corners, ret)
        #cv2.imshow("Corners%s"%fname,img)
        cv2.imshow("Corners",img)
        print("fname:%s, no:%s "%(fname, n))
        cv2.waitKey(0)
        n += 1
        #print(fname)
    else:
        print(fname) # Print name of problem-child image


N_OK = len(objpoints)
#objpoints = np.array([objpoints])
#imgpoints = np.array([imgpoints])

K = np.zeros((3, 3)) # create for calibration matrix (K)
D = np.zeros((4, 1)) # create distortion coefficients (D)
rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)] # rotation vectors estimated for each cal pattern
tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)] # translation vectors estimated for each cal pattern

# rms, K1, D1, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints,
#                                                        gray.shape[::-1], K, D, flags=calibration_flags,criteria = (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6))

rms, _, _, _, _ = \
    cv2.fisheye.calibrate(
        objpoints,
        imgpoints,
        gray.shape[::-1],
        K,
        D,
        rvecs,
        tvecs,
        calibration_flags,
        (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6)
    )


#rms, K, D, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1])
#
# print(len(imgpoints))
#
# rms, K, D, rvecs, tvecs = cv2.calibrateCamera(objpoints,imgpoints,gray.shape[::-1],None,None)


print("Found " + str(N_OK) + " valid images for calibration")
# print("DIM=" + str(_img_shape[::-1]))
# print("K=np.array(" + str(K.tolist()) + ")")
# print("D=np.array(" + str(D.tolist()) + ")")

DIM = _img_shape[::-1]

#save the numpy array of intrinsic and extrinsic cameraCalibration Parameters into a numpy array file
piCamCalParams = open("piCamCalParams.npz", "wb")
np.savez(piCamCalParams, DIM, K, D, rvecs, tvecs)
piCamCalParams.close()

# # Read From eyeCamCalMParams.npz
# piCamCalParams = np.load("piCamCalParams.npz")
# DIM2 = piCamCalParams['arr_0']
# K2 = piCamCalParams['arr_1']
# D2 = piCamCalParams['arr_2']
# 
# rvecs2 = piCamCalParams['arr_3']
# tvecs2 = piCamCalParams['arr_4']
# 
# print("DIM=" + str(DIM) + " DIM2=" + str(DIM2) + "\n")
# print("K=np.array(" + str(K.tolist()) + ")" + "K2=np.array(" + str(K2.tolist()) + ") \n")
# print("D=np.array(" + str(D.tolist()) + ")" + "D2=np.array(" + str(D2.tolist()) + ") \n")
# 
# #cv2.destroyAllWindows()
# 
# ##https://medium.com/@kennethjiang/calibrate-fisheye-lens-using-opencv-333b05afa0b0
# def undistort(img_path):
#     img = cv2.imread(img_path)
#     h,w = img.shape[:2]
#     map1, map2 = cv2.fisheye.initUndistortRectifyMap(K2, D2, np.eye(3), K2, tuple(DIM2), cv2.CV_16SC2)
#     undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
#     cv2.imshow("undistorted", undistorted_img)
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()
#     
# #for fname in images:
# #    undistort(fname)
# undistort('img00010.pgm')


#cv2.waitKey(100000)
cv2.destroyAllWindows()



