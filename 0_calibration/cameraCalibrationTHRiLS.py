import cv2
#assert cv2.__version__[0] == '3', 'The fisheye module requires opencv version >= 3.0.0'

import numpy as np
import os
import glob

##bad checkerboard
# squaremm = 25#10
# width = 6#5
# height = 9#8

# ##highexpcheckerboard
# squaremm = 19.28
# width = 11
# height = 7

##highreshighdensity
squaremm = 9.71
width = 25
height = 17

CHECKERBOARD = (width,height)

subpix_criteria = (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1)

#calibration_flags = cv2.CALIB_RECOMPUTE_EXTRINSIC+cv2.CALIB_CHECK_COND+cv2.CALIB_FIX_SKEW

#initialising object points in the checkerboard coordinate frame
objp = np.zeros((1, CHECKERBOARD[0]*CHECKERBOARD[1], 3), np.float32)
objp[0,:,:2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)

_img_shape = None
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

#images = glob.glob('4_checkerboard/*.pgm')
#images = glob.glob('../../0_RawCaptures/00_Calibration/HighExpCheckerboard/*.pgm')
#images = glob.glob('../../0_RawCaptures/00_Calibration/CalibrationHighResHighDensity/*.pgm')
images = glob.glob('../../0_RawCaptures/00_Calibration/HighDense2/*.pgm')
n = 0
for fname in images:

    img = cv2.imread(fname)
    if _img_shape == None:
        _img_shape = img.shape[:2]
    else:
        assert _img_shape == img.shape[:2], "All images must share the same size."

   # Convert image to opencv grayscale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_FAST_CHECK+cv2.CALIB_CB_NORMALIZE_IMAGE)

    # If found, add object points, image points (after refining them)
    if ret == True:

        objpoints.append(objp)
        cv2.cornerSubPix(gray,corners,(3,3),(-1,-1),subpix_criteria)
        corners = np.array([[corner for [corner] in corners]])
        imgpoints.append(corners)

        cv2.drawChessboardCorners(img,(width,height), corners, ret)
        #cv2.imshow("Corners%s"%fname,img)
        cv2.imshow("Corners",img)
        print("fname:%s, no:%s "%(fname, n))
        cv2.waitKey(0)
        n += 1
        #print(fname)
    else:
        print(fname) # Print name of problem-child image

N_OK = len(objpoints)

K = np.zeros((3, 3)) # create for calibration matrix (K)
D = np.zeros((4, 1)) # create distortion coefficients (D)
rvecs = [np.zeros((1, 1, 3), dtype=np.float32) for i in range(N_OK)] # rotation vectors estimated for each cal pattern
tvecs = [np.zeros((1, 1, 3), dtype=np.float32) for i in range(N_OK)] # translation vectors estimated for each cal pattern

print (len(objpoints))
print (len(imgpoints))

objpoints = np.array(objpoints)

# print (objpoints.size())
# print (imgpoints.shape())

#imgpoints =

rms, K, D, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

print(rms)
print("Found " + str(N_OK) + " valid images for calibration")
print("DIM=" + str(_img_shape[::-1]))
print("K=np.array(" + str(K.tolist()) + ")")
print("D=np.array(" + str(D.tolist()) + ")")

#save the numpy array of intrinsic and extrinsic cameraCalibration Parameters into a numpy array file
eyeCamCalParams = open("piCamCalParams.npz", "wb")
np.savez(eyeCamCalParams, _img_shape[::-1], K, D, rvecs, tvecs)
eyeCamCalParams.close()