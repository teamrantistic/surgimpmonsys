import cv2
#assert cv2.__version__[0] == '3', 'The fisheye module requires opencv version >= 3.0.0'

import numpy as np
import os
import glob

import datetime
import pytz

#function to get current time for timestamping images
def utcnow():
    return datetime.datetime.now(tz=pytz.utc)

#width = 6
#height = 9
squaremm = 25#10
width = 6#5
height = 9#8
CHECKERBOARD = (width,height)

#for interpolation obetween pixels
subpix_criteria = (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1)

calibration_flags = cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC+cv2.fisheye.CALIB_CHECK_COND+cv2.fisheye.CALIB_FIX_SKEW

#initialising object points in the checkerboard coordinate frame
objp = np.zeros((1, CHECKERBOARD[0]*CHECKERBOARD[1], 3), np.float64)
objp[0,:,:2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)

_img_shape = None
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.


images = glob.glob('4_checkerboard/*.pgm')
n = 0
for fname in images:

    img = cv2.imread(fname)
    if _img_shape == None:
        _img_shape = img.shape[:2]
    else:
        assert _img_shape == img.shape[:2], "All images must share the same size."

    # Convert image to opencv grayscale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_FAST_CHECK+cv2.CALIB_CB_NORMALIZE_IMAGE)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
        cv2.cornerSubPix(gray,corners,(3,3),(-1,-1),subpix_criteria)
        imgpoints.append(corners) 
        cv2.drawChessboardCorners(img,(width,height), corners, ret)
        #cv2.imshow("Corners%s"%fname,img)
        cv2.imshow("Corners",img)
        print("fname:%s, no:%s "%(fname, n))
        cv2.waitKey(0)
        n += 1
        #print(fname)
    else:
        print(fname) # Print name of problem-child image

N_OK = len(objpoints)

K = np.zeros((3, 3)) # create for calibration matrix (K)
D = np.zeros((4, 1)) # create distortion coefficients (D)
rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)] # rotation vectors estimated for each cal pattern
tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(N_OK)] # translation vectors estimated for each cal pattern

rms, _, _, _, _ = \
    cv2.fisheye.calibrate(
        objpoints,
        imgpoints,
        gray.shape[::-1],
        K,
        D,
        rvecs,
        tvecs,
        calibration_flags,
        (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-6)
    )
print(rms)
print("Found " + str(N_OK) + " valid images for calibration")
print("DIM=" + str(_img_shape[::-1]))
print("K=np.array(" + str(K.tolist()) + ")")
print("D=np.array(" + str(D.tolist()) + ")")


#save the numpy array of intrinsic and extrinsic cameraCalibration Parameters into a numpy array file
eyeCamCalParams = open("piCamCalParams.npz", "wb")
np.savez(eyeCamCalParams, _img_shape[::-1], K, D, rvecs, tvecs)
eyeCamCalParams.close()

#cv2.destroyAllWindows()

#3D pose
imgpoints2 = []
runningtotal_error = 0

for i in range(N_OK):
    img = cv2.imread(images[i])
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    realpoints_correctShape = np.zeros((35,1,2),np.float32)
    realpoints, _= cv2.fisheye.projectPoints(objpoints[i],rvecs[i],tvecs[i],K,D)
    realpointsarray = np.asarray(realpoints)

    for j in range(len(realpoints_correctShape)):
        realpoints_correctShape[j][0] = realpoints[0][j]
    #calculate the average error between all corners in one image frame (in pixels)
    #error = cv2.norm(imgpoints[i], realpoints_correctShape, cv2.NORM_L2)/len(realpoints_correctShape)
    #print("Avrgerrorin1frame: %s, %s"%(error,i))
    #calculate and print the avrg error over all frames (in pixels)
    #runningtotal_error += error
    #currentmean_error = runningtotal_error/N_OK
    #print( "current avrg. reproj error: %s"%currentmean_error)
    
    imgpoints2.append(realpoints_correctShape)
    #print("%s --> %s"%(imgpoints[i],imgpoints2[i]))
    if i == 2 or i == 3 or i == 4 or i == 5 or i == 19 or i == 27 or i == 31:
        cv2.drawChessboardCorners(img,(width,height), imgpoints2[i], 1)
        cv2.drawChessboardCorners(img,(width,height), imgpoints[i], 1)
        cv2.imshow("Projection %s"%i,img)
        cv2.waitKey(0)

#cv2.waitKey(100000)
cv2.destroyAllWindows()


