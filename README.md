# Surgical Impaction Monitoring System for Project THRiLS 

## Table of Contents

* [System Setup](#markdown-header-system-setup)
	* [Arduino](#markdown-header-arduino)
	* [Raspberry Pi and Camera](#markdown-header-raspberry-pi-and-camera)
	* [Omni](#markdown-header-omni)
	* [PC](#markdown-header-pc)
	
* [System Details](#markdown-header-system-details)
	* [Arduino](#markdown-header-arduino-info)
	* [Raspberry Pi](#markdown-header-raspberry-pi-info)
	* [Camera](#markdown-header-camera-info)
	* [Omni - Touch Haptic Device](#markdown-header-omni-info)
	* [PC](#markdown-header-pc-info)
	
* [Surgical Tool Setup](#markdown-header-surgical-tool-setup)

- - -

# System Setup

![alt text](images/atHomeImpactionJig.svg "systemDiagram")

[comment]: <> (![Iframe]&#40;"images/atHomeImpactionJig.drawio.html"&#41;)
* [Back to TOC](#markdown-header-table-of-contents)

- - -
- - -
- - -
- - -
- - -
- - -
- - -
- - -
- - -

## Arduino


* [Back to TOC](#markdown-header-table-of-contents)

- - -
- - -


## Raspberry Pi Camera

Raspberry pi 4 used


- - -
## Omni
- - -

- - -
## PC

# System Details 

## Arduino Info
The Arduino is responsible for:

* Generating synchronous (square-wave) trigger pulses to control:
	* Raspberry Pi Camera frame capture (100Hz)
	* Touch Haptic Device end-effector position and orientation (6DOF) capture (50Hz-2000Hz)
	* Analogue-to-Digital Converter (ADC) clock for force sensor data capture (2000Hz)
* Serial Communication with Raspberry Pi to:
	* Send/Receive commands
	* Save/Send force sensor data capture
* Electromagnet Engage/Disengage Control for automated mallet dropping

### Arduino Hardware
Arduino used in this project is an Arduino Due which has a at91sam3x8e microcontroller.

#### Key Features
* I/O Levels : 3V3
* Native USB Port
* ADC: 12-Bit

## Raspberry Pi Info

The Raspberry Pi is responsible for:
* Interfacing with (Camera Serial Interface: CSI)
* Live Preview of camera
* Capturing frames from the Camera of:
	* Calibration Patterns
	* Mallet swing and impaction with introducer
* Serial Communication with the Arduino
	* Send/Receive Commands
	* Receive Force Sensor Data and save as .csv
	* Comparing no. of captured frames with the pulse count received from the Arduino to determine whether any frames have been dropped
* TCP/IP port communication with Omni's PC:
	* Send/Receive commands (mainly the experiment name for folder access)
* Generation of Folder structure, saving to USB flash-drive and experiment naming convention (with unique time-stamp) for each test
 
### Raspberry Pi Hardware
Raspberry Pi 4 (Model B) with 8GB RAM is used.
* Wifi

#### Peripherals
* Screen - to visualise live feed of camera.
	

## Camera Info
The Camera used is an OV9281

### Camera Hardware
OV9281 Innomaker

#### Key Features
* Global Shutter
* External Trigger
* Resolution: 640x400
* Frame Rate : Max 250fps
* .pgm (portable grayscale mapping format)
* Y8 or Y10
* innomaker drivers for Raspberry Pi 4

## Omni Info
Formerly known as the Phantom Omni (and is referred to as omni in this project), the Touch haptic device is a desktop articulated arm used mainly for programmable haptic feedback. However, for this project, the device is only used as a gold-standard positioning device and the haptic feedback features are not used.

In this project the Omni is used:
* To capture positioning of the stylus
* With a 3D-printed adapter afixed to the stylus for connecting the surgical mallet

### Omni Hardware
Touch Haptic Device
Power cable
USB3

#### Key Features
* Max Force: ...
* Positioning accuracy and precision values : +/- ....
* 6DOF
* 3 Haptic feedback joints
* Grippable Stylus
* OpenHaptics API

### Omni Software
* Open Haptics API
* Visual Studio C++ used to modify installation

## PC Info
Responsible for:
* Interfacing with the Omni through USB3
* Saving Omni data in .csv format
* Communicating with Raspberry Pi over TCP/IP
* Processing and Analysis of data collected from all sensors
* Data visualisation of post-process data

### PC Hardware
PC used for this project:
* Storage size: ...
* RAM: 32GB
* separate graphics card - Nvidia RTX 2080
* USB 3 port
* Wifi


### PC Software
* Operating System: Windows 10 PC
* PyCharm
* Visual Studio 2019 - Link to the visual studio installer config here

# Surgical Tool Setup

## Tools
* Surgical Mallet
* Surgical Introducer




















