#!/usr/bin/env python3
import serial
import time

##https://roboticsbackend.com/raspberry-pi-arduino-serial-communication/#Raspberry_Pi_Software_setup

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.flush()
    while True:
        ser.write(b"200")
        line = ser.readline().decode('utf-8').rstrip()
        print(line)
        time.sleep(10)
