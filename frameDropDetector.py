import cv2
import numpy as np
import os
from matplotlib import pyplot as plt

dropCount = 0
ledStatePrev = 0
ledStateCurr = 0

prevFrame = cv2.imread("/mnt/ramdisk/img00000.pgm", cv2.IMREAD_UNCHANGED)
_ ,prevFrameBin = cv2.threshold(prevFrame,127,255,cv2.THRESH_BINARY)

ledOnOffEstimatePrev, _, _, _ = cv2.mean(prevFrameBin)
if ledOnOffEstimatePrev == 0.0:
    ledStatePrev = 0
else:
    ledStatePrev = 1
print (ledOnOffEstimatePrev)
print (ledStatePrev)
cv2.imshow("frameOriginal", prevFrame)
cv2.imshow("frameThresh", prevFrameBin)
#plt.hist(prevFrameBin.ravel(),256,[0,256]); plt.show()

cv2.waitKey()

##for the first 2000 frames

for i in range(2000):
    filename = "/mnt/ramdisk/img" + str(i+1).zfill(5) + ".pgm"
    currFrame = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
    _ ,currFrameBin = cv2.threshold(currFrame,127,255,cv2.THRESH_BINARY)

    ledOnOffEstimateCurr, _, _, _ = cv2.mean(currFrameBin)
    
    if ledOnOffEstimateCurr == 0.0:
        ledStateCurr = 0
    else:
        ledStateCurr = 1


    if ledStateCurr == ledStatePrev:
        dropCount = dropCount + 1
        print (dropCount)
    else:
        print (filename + str(ledStateCurr))

    prevFrame = currFrame
    ledStatePrev = ledStateCurr
print("totalDroppedFrames: " + str(dropCount))
#    cv2.imshow("frame", currFrame)
#    cv2.waitKey()

