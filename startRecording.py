import cv2
import numpy as np
import subprocess
import os
#import sys
import re
import math
import time

##setup serial comms with arduino
import serial
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
ser.flush()

##attempt to change directory to ramdisk Memory. Exit if cannot
result2 = os.chdir("/mnt/ramdisk")
print(result2)
if result2 != None:
    print("could not execute successfully error: ", result2)
    exit()

folder_contents = subprocess.run(["ls"], capture_output = True, text = True)

#print("folder content: ", folder_contents.stdout)

if re.search("vcmipidemo", folder_contents.stdout):
    print("vcmipidemo already exists...skipping copy")
else:
    copy_result = subprocess.run(["cp", "/home/pi/surgicalimpactionmonitoringsystem/1_ClientSide_ThePC/Captures/vcmipidemo", "/mnt/ramdisk/"])
    print("copy errors? : %d" % copy_result.returncode)
    if copy_result.returncode != 0:
        print("could not copy! .. exiting with error: ", copy_result)
        exit()

noOfPulses = 1000 #240
#noOfRuns = 20
freq = 100 #80
#freqStart = 25
#freqIncr = 5
#freqEnd = 200 + freqIncr


#dataFileName = "GUIFrameDropsOver"+str(noOfPulses)+"_"+str(noOfRuns)+"Runs"

#dropCountArray = np.zeros(int(noOfRuns), dtype=int)

##run vcmipidemo
openCamChildProcess = subprocess.Popen(["./vcmipidemo", "-s", "261630", "-g", "0x88", "-f", "-b", "32", "-a", "-o"], stdout=None, stderr=None)

time.sleep(1)

##delete any existing image files
list_of_files = os.listdir("/mnt/ramdisk")
for frame in list_of_files:
    if frame.endswith(".pgm"):
        os.remove(os.path.join("/mnt/ramdisk/", frame))

startRecording = input("Start recording? y/n: ")
        #print("usr_input was : ", user_confirm)
    #    freq_req = input("please input test freq and no. of pulses ... freq,pulses;: ")
        #print (freq_req)
        #time.sleep(1)

sendCommandArduino = str(freq) + "," + str(noOfPulses) + ";"

##set arduino freq
ser.write(sendCommandArduino.encode('utf_8'))

##read from arduino
ser.timeout = None
freq_closest = ser.readline().decode('utf-8').rstrip()
print(freq_closest)
completionFlag = ser.readline().decode('utf-8').rstrip()
print(completionFlag)
openCamChildProcess.terminate()
frameCount = 0
list_of_files = os.listdir("/mnt/ramdisk")
for frame in list_of_files:
    if frame.endswith(".pgm"):
        frameCount = frameCount + 1;
        
droppedFrames = noOfPulses - frameCount
#dropCountArray[j] = droppedFrames
#        dropCountArray[j+1] = droppedFrames

print(droppedFrames)
    
    # #file_name = str(freq) + ".npz"
# file_name = "/home/pi/surgicalimpactionmonitoringsystem/autoFrameDropResults/" + str(freq) + ".npz"
# print("drops in all tests ", dropCountArray)
# dropCountFile = open(file_name, "wb")
# np.savez(dropCountFile,dropCountArray)
# dropCountFile.close()
    # #currTest = np.load(file_name)
    # #testResults = currTest['arr_0']
    # #print(testResults)

# currTest = np.load(file_name)
# testResults = currTest['arr_0']
# print(testResults)

#print ("measuring current frequency .... ")

#freq_meas = subprocess.run(["v4l2-ctl", "--stream-mmap", "--stream-count=1250", "-d", "/dev/video0", "--stream-to=/dev/null"], capture_output = True, text = True)

##freq_meas = subprocess.run(["v4l2-ctl", "--stream-mmap", "--stream-count=1250", "-d", "/dev/video0", "--stream-to=/dev/null"])
##print (freq_meas.stderr)

#if re.search(freq_req, freq_meas.stderr):
#    print("match found!")
#else:
#    print("no match: ", freq_meas.stderr)
#    quit()


#list_of_strings = freq_meas.stderr.split('fps\n')
#print(list_of_strings)
#user_confirm = input("correct freq y/n: ")
#if user_confirm == 'n':
#   exit()

 #print ("the read output: ", freq_meas.stdout)
# timeout_secs = (math.ceil((2000.00/int(freq_req))))+5
# 
# no_of_runs = input("How many times would you like the test to run (an integer)?: ")
# 
# dropCountArray = np.zeros(int(no_of_runs), dtype=int)
# 
# for run in range(int(no_of_runs)):
#     list_of_files = os.listdir("/mnt/ramdisk")
#     for frame in list_of_files:
#         if frame.endswith(".pgm"):
#             os.remove(os.path.join("/mnt/ramdisk/", frame))
#     ##capture frames
#     try:
#         subprocess.run(["./vcmipidemo", "-s", "261630", "-g", "0x88", "-f", "-b", "32", "-a", "-o"], timeout= timeout_secs)
#         
#     except subprocess.TimeoutExpired:
#         print("timeout done after seconds: ", timeout_secs)
# 
#     dropCount = 0
#     ledStatePrev = 0
#     ledStateCurr = 0
# 
#     prevFrame = cv2.imread("img00000.pgm", cv2.IMREAD_UNCHANGED)
#     _ ,prevFrameBin = cv2.threshold(prevFrame,200,255,cv2.THRESH_BINARY)
# 
#     ledOnOffEstimatePrev, _, _, _ = cv2.mean(prevFrameBin)
#     if ledOnOffEstimatePrev == 0.0:
#         ledStatePrev = 0
#     else:
#         ledStatePrev = 1
# #print (ledOnOffEstimatePrev)
# #print (ledStatePrev)
# #cv2.imshow("frameOriginal", prevFrame)
# #cv2.imshow("frameThresh", prevFrameBin)
# ##plt.hist(prevFrameBin.ravel(),256,[0,256]); plt.show()
# 
# #cv2.waitKey()
# 
# ##for the first 2000 frames
# 
#     for i in range(2000):
#         filename = "img" + str(i+1).zfill(5) + ".pgm"
#         currFrame = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
#         _ ,currFrameBin = cv2.threshold(currFrame,200,255,cv2.THRESH_BINARY)
# 
#         ledOnOffEstimateCurr, _, _, _ = cv2.mean(currFrameBin)
#         print("mean val: ", ledOnOffEstimateCurr) 
#         if ledOnOffEstimateCurr == 0.0:
#             ledStateCurr = 0
#         else:
#             ledStateCurr = 1
# 
# 
#         if ledStateCurr == ledStatePrev:
#             dropCount = dropCount + 1
#             print (filename + str(ledStateCurr))
# #            print (dropCount)
# #        else:
# 
#         prevFrame = currFrame
#         ledStatePrev = ledStateCurr
#     print("totalDroppedFrames: " + str(dropCount))
#     dropCountArray[run] = dropCount
# #    cv2.imshow("frame", currFrame)
# #    cv2.waitKey()
# file_name = "/home/pi/surgicalimpactionmonitoringsystem/" + freq_req + ".npz"
# print("drops in all tests ", dropCountArray)
# dropCountFile = open(file_name, "wb")
# np.savez(dropCountFile, dropCountArray)
# dropCountFile.close()
